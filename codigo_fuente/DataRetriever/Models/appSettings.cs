﻿using ServiceLibroPatrono.parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataRetriever.Models
{
    public sealed class appSettings
    {
        public int recordsPerRequest { get; set; }
        public string rootPDF { get; set; }
        public string eLibroPatronoURL { get; set; }
        public concurrentSubscribers concurrentSubscribers { get; set; }
        public domainSettings domainSettings { get; set; }
    }
}
