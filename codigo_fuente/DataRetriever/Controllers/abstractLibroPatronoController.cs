﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataRetriever.Models;
using DMLibroPatrono.interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DataRetriever.Controllers
{
    public abstract class abstractPatenteController<TModel, Tkey> : ControllerBase
    {
        private readonly Func<IWorkFlow<TModel>> workFlow;
        private readonly IOptions<appSettings> appSettings;
        private readonly Func<IPubStartSigner<Tkey>> startSignerFactory;

        protected abstractPatenteController(Func<IWorkFlow<TModel>> workFlow,
                                            Func<IPubStartSigner<Tkey>> startSignerFactory,
                                            IHostingEnvironment hostingEnvironment,
                                            IOptions<appSettings> appSettings)
        {
            this.workFlow = workFlow;
            this.appSettings = appSettings;
        }

        protected async Task<IActionResult> _get(int lote, string user)
        {
            try
            {
                var appDomain = workFlow();
                var data = await appDomain.documentosParaFirma(null, appSettings.Value.recordsPerRequest, HttpContext.Connection.RemoteIpAddress, user);
                return Ok(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error on GET " + ex.ToString());
                return BadRequest(ex.ToString());
            }
        }

        protected async Task<IActionResult> _post(IEnumerable<Guid> patentes)
        {
            try
            {
                if (patentes.Any())
                {
                    var remoteIpAddress = HttpContext.Connection.RemoteIpAddress;
                    var pubSigner = startSignerFactory();
                    await pubSigner.startSigner(patentes, remoteIpAddress, "tempUser");
                    return Ok();
                }
                return BadRequest("No se indicaron documentos a procesar");
            }
            catch (Exception ex)
            {
                return BadRequest("No se procesaron los documentos " + ex.ToString());
            }
        }
    }
}