﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataRetriever.Middleware;
using DataRetriever.Models;
using DMLibroPatrono;
using DMLibroPatrono.interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DataRetriever.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [validateModel]
    public class libroPatronoController : abstractPatenteController<modelLibroPatrono,identificacionLibroPatrono>
    {
        protected libroPatronoController(Func<IWorkFlow<modelLibroPatrono>> workFlow, 
            Func<IPubStartSigner<identificacionLibroPatrono>> startSignerFactory, 
            IHostingEnvironment hostingEnvironment, 
            IOptions<appSettings> appSettings) : base(workFlow, startSignerFactory, hostingEnvironment, appSettings)
        {
        }

        [HttpGet("paraFirma/{lote:int}/{user:alpha}")]
        public async Task<IActionResult> get(int lote, string user)
        {
            return await _get(lote, user);
        }
    }
}
