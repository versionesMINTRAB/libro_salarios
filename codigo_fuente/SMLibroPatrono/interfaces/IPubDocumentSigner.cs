﻿using SMLibroPatrono.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SMLibroPatrono.interfaces
{
    public interface IPubDocumentSigner<TModel>
    {
        void signDocument(modelUnsignedFile file);
    }
}
