﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SMLibroPatrono.interfaces
{
    public interface IPubUnsignedDoc<T>
    {
        void makeDocument(T document);
    }
}
