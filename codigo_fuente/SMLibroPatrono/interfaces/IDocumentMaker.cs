﻿using DMLibroPatrono.interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SMLibroPatrono.interfaces
{
    public interface IDocumentMaker<TModel, TResult>
        where TResult : IDocumentoGenerado
    {
        Task<TResult> makeDocument(TModel dataModel, string rootSigner, Guid batchId, Guid token);
    }
}
