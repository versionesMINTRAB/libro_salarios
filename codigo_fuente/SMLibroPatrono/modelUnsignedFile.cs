﻿using commonModel;
using commonModel.interfaces;
using DMLibroPatrono.batch;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace SMLibroPatrono.common
{
    public sealed class modelUnsignedFile : IMessage
    {
        public modelDocumentoGenerado documentoGenerado { get; set; }
        public Guid contexto { get; set; }
        [JsonConverter(typeof(ipAddressConverter))]
        public IPAddress ip { get; set; }
        public string user { get; set; }
    }
}
