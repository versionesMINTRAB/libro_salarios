﻿using commonModel;
using DMLibroPatrono.interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace SMLibroPatrono
{
    public sealed class modelUnsignedDocument<TModel> : IGenMessage<TModel>
    {
        public Guid token { get; set; }
        public TModel document { get; set; }
        public Guid contexto { get; set; }
        public string rootSigner { get; set; }
        [JsonConverter(typeof(ipAddressConverter))]
        public IPAddress ip { get; set; }
        public string user { get; set; }
    }
}
