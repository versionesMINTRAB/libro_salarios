﻿using commonModel.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SMLibroPatrono
{
    public sealed class modelSignRemoteCall : IMessage
    {
        public string rootPath { get; set; }
        public string coordenadas { get; set; }
        public string pathRubrica { get; set; }
        public string clientIP { get; set; }
        public Guid contexto { get; set; }
    }
}
