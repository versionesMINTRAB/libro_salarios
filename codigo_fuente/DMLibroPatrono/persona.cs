﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMLibroPatrono
{
    public sealed class persona
    {
        public bool loadFromXML(string xml)
        {
            try
            {
                var tmp = new System.Xml.XmlDocument();
                tmp.LoadXml(xml);
                if (tmp.SelectSingleNode("CONTRIBUYENTE/COD").InnerText.Equals("0") && tmp.SelectSingleNode("CONTRIBUYENTE/NOM") != null)
                {
                    nit = tmp.SelectSingleNode("CONTRIBUYENTE/NIT").InnerText;
                    nombre = tmp.SelectSingleNode("CONTRIBUYENTE/NOM").InnerText;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public string nit { get; set; }
        public string nombre { get; set; }
        
    }
}
