﻿using DMLibroPatrono.batch;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DMLibroPatrono.interfaces
{
    public interface IFirma<TKey, TModelPIN>
    {
        Task addDocumentToBatch(Guid batchID, TKey documento);
        Task removeDocumento(Guid id);
        Task addBatch(Guid id, IPAddress ip, int batchSize, string rootSigner, string user);
        Task<modelLoteFirma> getJobFirma(Guid id);
        Task<IEnumerable<TKey>> getPatentesByBatchID(Guid BatchID);
        Task uploadSignedFile(string user, int status, string descripcion, Guid batchId, modelDocumentoFirmado documentoFirmada, TModelPIN pin);
        Task uploadUnsignedFile(string user, int status, string descripcion, Guid id, Guid batchId, IPAddress ip, DateTimeOffset fecha);
        Tuple<int, IEnumerable<modelFirma>> verifySignatures(string path);
        Task<int> getJobCounter(Guid id);
        Task incrementJobCounter(Guid id);
        Task<bool> fileUploaded(Guid id);
    }
}
