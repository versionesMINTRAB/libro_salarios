﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DMLibroPatrono.interfaces
{
    public interface IPubStartSigner<TKey>
    {
        Task startSigner(IEnumerable<Guid> patentes, IPAddress clientIP, string user);
    }
}
