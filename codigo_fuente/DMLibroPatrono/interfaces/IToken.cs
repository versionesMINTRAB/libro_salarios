﻿using System;

namespace DMLibroPatrono.interfaces
{
    public interface IToken
    {
        Guid batchId { get; set; }
        Guid id { get; set; }
    }
}
