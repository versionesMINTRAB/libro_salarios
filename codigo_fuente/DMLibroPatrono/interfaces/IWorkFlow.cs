﻿using DMLibroPatrono.enums;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DMLibroPatrono.interfaces
{
    public interface IWorkFlow<TModel>
    {
        Task<IEnumerable<Tuple<Guid, TModel>>> documentosParaFirma(byte[] lote, int recordsPerRequest, IPAddress ip, string user);
        Task signDocument(TModel document, Guid id, IPAddress ip, string user);
        Task<IEnumerable<TModel>> retrieveByStatus(signStatus status, byte[] page, IPAddress ip, string user);
        Task<TModel> retrieveByID(signStatus status, Guid id, IPAddress ip, string user);
    }
}
