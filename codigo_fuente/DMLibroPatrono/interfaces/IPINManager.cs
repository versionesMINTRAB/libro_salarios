﻿using System;
using System.Threading.Tasks;

namespace DMLibroPatrono.interfaces
{
    public interface IPINManager<TModel, TModelPIN>
    {
        Task<TModel> documentByToken(Guid token);
        Task addPIN(TModelPIN modelPIN);
        Task<TModelPIN> getPIN(TModel modelPatente);
        Task<byte[]> documentoFisico(TModelPIN pin);
        Task<Tuple<TModel, bool>> patenteByPIN(TModelPIN pin);
    }
}
