﻿using commonModel.interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DMLibroPatrono.interfaces
{
    public interface IGenMessage<TKey> : IMessage
    {
        TKey document { get; set; }
        Guid token { get; set; }
        string rootSigner { get; set; }
        IPAddress ip { get; set; }
        string user { get; set; }
    }
}
