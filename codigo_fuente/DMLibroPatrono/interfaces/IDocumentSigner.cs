﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DMLibroPatrono.interfaces
{
    public interface IDocumentSigner<TModel>
    {
        modelSignResult signDocuments(string archivo, string coordenadas, string pathRubrica, IPAddress clientIP);
    }
}
