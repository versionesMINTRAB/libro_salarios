﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMLibroPatrono.interfaces
{
    public interface ILibroPatrono<TKey>
    {
        TKey id { get; set; }
    }
}
