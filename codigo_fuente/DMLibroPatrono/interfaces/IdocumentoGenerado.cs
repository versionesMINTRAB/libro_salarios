﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMLibroPatrono.interfaces
{
    public interface IDocumentoGenerado
    {
        Guid key { get; set; }
        string path { get; set; }
    }
}
