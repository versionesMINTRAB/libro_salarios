﻿using DMLibroPatrono.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DMLibroPatrono
{
    public class modelTokenLibroPatrono : IToken
    {
        public Guid batchId { get; set; }
        public DateTimeOffset fecha { get; set; }
        public string nit { get; set; }
        public Guid id { get; set; }

        public static explicit operator identificacionLibroPatrono(modelTokenLibroPatrono x)
        {
            return new identificacionLibroPatrono()
            {
                fecha = x.fecha,
                nit = x.nit
            };
        }
    }
}
