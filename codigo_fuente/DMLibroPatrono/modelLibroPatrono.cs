﻿using DMLibroPatrono.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DMLibroPatrono
{
    public sealed class modelLibroPatrono : ILibroPatrono<identificacionLibroPatrono>
    {
        public identificacionLibroPatrono libroPatrono { get; set; }
        public string nombrePatrono { get; set; }
        public int cantidadFolios { get; set; }
        public string replyToAddress { get; set; }
        public identificacionLibroPatrono id { get => libroPatrono; set => libroPatrono = value; }
    }
}
