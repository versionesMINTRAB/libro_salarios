﻿using DMLibroPatrono.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DMLibroPatrono.batch
{
    public class modelDocumentoGenerado : IDocumentoGenerado
    {
        public Guid key { get; set; }
        public string path { get; set; }
    }
}
