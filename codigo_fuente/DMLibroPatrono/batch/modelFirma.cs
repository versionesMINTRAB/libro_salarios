﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMLibroPatrono.batch
{
    public sealed class modelFirma
    {
        public string nombreFirma { get; set; }
        public string firmante { get; set; }
        public string razon { get; set; }
        public bool cubreTodoElDocumento { get; set; }
        public int revision { get; set; }
        public bool esIntegra { get; set; }
    }
}
