﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DMLibroPatrono.batch
{
    public sealed class modelDocumentoFirmado : modelDocumentoGenerado
    {
        public int revisionesDocumento { get; set; }
        public IPAddress ipOrigen { get; set; }
        public DateTimeOffset fechaFirma { get; set; }
        public IEnumerable<modelFirma> firmas { get; set; }
    }
}
