﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DMLibroPatrono.batch
{
    public class modelLoteFirma
    {
        public Guid id { get; set; }
        public IPAddress ip { get; set; }
        public string user { get; set; }
        public DateTimeOffset startedAt { get; set; }
        public int batchSize { get; set; }
        public string rootSigner { get; set; }
    }
}
