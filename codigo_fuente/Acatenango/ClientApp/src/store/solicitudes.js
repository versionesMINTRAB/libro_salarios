﻿const requestSolicitudes = 'REQUEST_SOLICITUDES';
const receiveSolicitudes = 'RECEIVE_SOLICITUDES';
const selectSolicitud = 'SELECT_SOLICITUD';
const unSelectSolicitud = 'UNSELECT_SOLICITUD';
const selectAll = 'SELECT_ALL_SOLICITUDES';
const unSelectAll = 'UNSELECT_ALL_SOLICITUDES';

const initialState = { solicitudes: [], isLoading: false, selected: [] };

export const actionCreators = {
    requestSolicitudes: pagina => async (dispatch, getState) => {
        if (pagina === getState().solicitudes.pagina) {
            return;
        }
        dispatch({ type: requestSolicitudes, pagina });

        const url = `api/libroPatrono/solicitudes/${pagina}`;
        const response = await fetch(url);
        const result = await response.json();
        dispatch({ type: requestSolicitudes, pagina, result });
    },
    selectSolicitud: row => ({ type: selectSolicitud, r: row }),
    unSelectSolicitud: row => ({ type: unSelectSolicitud, r: row }),
    selectAllSolicitudes: rows => ({ type: selectAll, r: rows }),
    unSelectAllSolicitudes: () => ({ type: unSelectAll })
};

export const reducer = (state, action) => {
    state = state || initialState;
    switch (action.type) {
        case requestSolicitudes:
            return { ...state, pagina: action.pagina, isLoading: true };
        case receiveSolicitudes:
            return { ...state, pagina: action.pagina, solicitudes: action.solicitudes, isLoading: false };
        case selectSolicitud:
            const clone = state.selected.slice(0);
            clone.push(action.r);
            return { ...state, selected: clone };
        case unSelectSolicitud:
            const result = state.selected.filter(r => !(r === action.r));
            return { ...state, selected: result };
        case selectAll:
            return { ...state, selected: action.r };
        case unSelectAll:
            return { ...state, selected: [] };
    }
    return state;
};