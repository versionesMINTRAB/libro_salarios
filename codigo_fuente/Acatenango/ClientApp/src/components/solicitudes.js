﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/solicitudes';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter, numberFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';

const products = [];

function addProducts(quantity) {
    const startId = products.length;
    for (let i = 0; i < quantity; i++) {
        const id = startId + i;
        products.push({
            id: id,
            name: 'Item name ' + id,
            price: 2100 + i
        });
    }
}

class solicitudes extends Component {
    componentWillMount() {
        addProducts(4);
    }

    render() {
        const CaptionElement = () => <h3 style={{ borderRadius: '0.25em', textAlign: 'center', color: 'skyblue', border: '1px solid darkgrey', padding: '0.5em' }}>Solicitudes de firma</h3>;
        const customTotal = (from, to, size) => (
            <span className="react-bootstrap-table-pagination-total">
                &nbsp;Mostrando {from} de {to} de un total de {size} registros
    </span>
        );
        const columns = [{
            dataField: 'id',
            text: 'NIT'
        }, {
            dataField: 'name',
            text: 'Nombre',
            sort: true,
            filter: textFilter()
        }, {
            dataField: 'price',
            text: 'Cantidad de Folios',
            filter: numberFilter()
        }];

        const pagerOptions = {
            paginationSize: 5,
            pageStartIndex: 0,
            // alwaysShowAllBtns: true, // Always show next and previous button
            // withFirstAndLast: false, // Hide the going to First and Last page button
            // hideSizePerPage: true, // Hide the sizePerPage dropdown always
           // hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
            firstPageText: 'Primero',
            prePageText: 'Anterior',
            nextPageText: 'Siguiente',
            lastPageText: 'Último',
            nextPageTitle: 'Ir a la siguiente pagina',
            prePageTitle: 'Volver a la pagina anterior',
            firstPageTitle: 'Primer pagina',
            lastPageTitle: 'Última pagina',
            showTotal: true,
            paginationTotalRenderer: customTotal
        };

        const selectRow = {
            mode: 'checkbox',
            clickToSelect: true,
            style: { backgroundColor: '#c8e6c9' },
            selected: this.props.selected,
            onSelect: (row, isSelect, rowIndex, e) => {
                if (isSelect) {
                    this.props.selectSolicitud(row.id);
                }
                else {
                    this.props.unSelectSolicitud(row.id);
                }
            },
            onSelectAll: (isSelect, rows, e) => {
                if (isSelect) {
                    this.props.selectAllSolicitudes(rows.map(x => x.id));
                }
                else {
                    this.props.unSelectAllSolicitudes();
                }
            }
        };

        return (
            <div>
                <BootstrapTable keyField='id'
                    columns={columns}
                    caption={<CaptionElement />}
                    data={products}
                    noDataIndication="No hay documentos pendientes de firma"
                    striped hover condensed
                    filter={filterFactory()}
                    selectRow={selectRow}
                    pagination={paginationFactory(pagerOptions)}
                />
            </div>
        );
    }
}

export default connect(
    state => state.solicitudes,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(solicitudes);