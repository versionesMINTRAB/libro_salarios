﻿using iText.Forms;
using iText.Forms.Fields;
using iText.IO.Font.Constants;
using iText.IO.Image;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Element;
using System;


namespace formGenerator
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //createFormSello();
            mergeFormSello();
            //createFormPatenteSociedad();
        }

        static void fun(object x)
        {
            Console.WriteLine(1);
        }

        static void fun(int x)
        {
            Console.WriteLine(1);
        }

        static void fun(double x)
        {
            Console.WriteLine(1);
        }

        private static string firstUpperCase(string text)
        {
            return text.Substring(0, 1).ToUpper() + text.Substring(1).ToLower();
        }

        
        private static void mergeFormSello()
        {
            const string selloForm = @"C:\Users\gpisqui\Documents\pdf\patenteEmpresa.pdf";
            const string pdfRoot = @"C:\Users\gpisqui\Documents\pdf\";
            var key = Guid.NewGuid();
            var fileName = pdfRoot + key.ToString() + ".pdf";
            var pdfDocument = new PdfDocument(new PdfReader(selloForm), new PdfWriter(fileName));
            var form = PdfAcroForm.GetAcroForm(pdfDocument, false);
            form.SetGenerateAppearance(true);
            var fontTimesRoman = PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN);
            form.GetField("boleta").SetValue("10,129");
            form.GetField("nit").SetValue("10243026-8");
            form.GetField("nombre").SetValue("GUILLERMO ALEXANDER PISQUI FUENTES");
            form.GetField("cantidadFolios").SetValue("786");
            form.GetField("fechaSolicitud").SetValue("14/11/2018 - 09:15:23");
            form.GetField("fechaAutorizacion").SetValue("19/11/2018 - 10:17:23");
          
            form.FlattenFields();
            pdfDocument.Close();
            Console.WriteLine("terminanos con el sello");
        }
      

        
        private static void createFormPatenteSociedad()
        {
            const string patenteImage = @"C:\Users\gpisqui\Documents\BITBUCKET\libro_salarios\Diseño_sello\Sello_Dirección_Gral_de_Trbajo-01.jpg";
            const string pdfRoot = @"C:\dev\RegistroMercantil\PDFRoot\";
            var key = Guid.NewGuid();
            var fileName = pdfRoot + key.ToString() + ".pdf";
            var pdfWriter = new PdfWriter(fileName);
            var pdfDocument = new PdfDocument(pdfWriter);
            var pageSize = PageSize.A4.Rotate();
            var document = new Document(pdfDocument, pageSize);
            var canvas = new PdfCanvas(pdfDocument.AddNewPage());
            canvas.AddImage(ImageDataFactory.CreateJpeg(new Uri(patenteImage)), pageSize, false);

            var form = PdfAcroForm.GetAcroForm(pdfDocument, true);
            var fontTimesRoman = PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN);

            var rctNo = new Rectangle(700, 435, 120, 20);
            var tfNo = PdfFormField.CreateText(pdfDocument, rctNo, "boleta", string.Empty, fontTimesRoman, 15).SetJustification(2);
            form.AddField(tfNo);

            var rctNom = new Rectangle(57, 407, 728, 26);
            var tfNom = PdfFormField.CreateText(pdfDocument, rctNom, "nombre", string.Empty, fontTimesRoman, 12).SetJustification(1);
            form.AddField(tfNom);

            var rcReg = new Rectangle(245, 375, 167, 20);
            var tfReg = PdfFormField.CreateText(pdfDocument, rcReg, "registro", string.Empty, fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfReg);

            var rcFolio = new Rectangle(439, 375, 130, 20);
            var tfFolio = PdfFormField.CreateText(pdfDocument, rcFolio, "folio", string.Empty, fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfFolio);

            var rcLibro = new Rectangle(597, 375, 108, 20);
            var tfLibro = PdfFormField.CreateText(pdfDocument, rcLibro, "libro", string.Empty, fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfLibro);

            var rcExpediente = new Rectangle(170, 350, 262, 20);
            var tfExpediente = PdfFormField.CreateText(pdfDocument, rcExpediente, "expediente", string.Empty, fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfExpediente);

            var rcNacionalidad = new Rectangle(500, 350, 320, 20);
            var tfNacionalidad = PdfFormField.CreateText(pdfDocument, rcNacionalidad, "nacionalidad", string.Empty, fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfNacionalidad);

            var rcFechaInscripcionDia = new Rectangle(176, 324, 217, 20);
            var tfFechaInscripcionDia = PdfFormField.CreateText(pdfDocument, rcFechaInscripcionDia, "fechaInscripcionDia", "16", fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfFechaInscripcionDia);

            var rcFechaInscripcionMes = new Rectangle(402, 324, 217, 20);
            var tfFechaInscripcionMes = PdfFormField.CreateText(pdfDocument, rcFechaInscripcionMes, "fechaInscripcionMes", "Marzo", fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfFechaInscripcionMes);

            var rcFechaInscripcionAnio = new Rectangle(620, 324, 187, 20);
            var tfFechaInscripcionAnio = PdfFormField.CreateText(pdfDocument, rcFechaInscripcionAnio, "fechaInscripcionAnio", "2018", fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfFechaInscripcionAnio);

            var rcDireccion = new Rectangle(170, 286, 650, 40);
            var tfDireccion = PdfFormField.CreateText(pdfDocument, rcDireccion, "direccion", string.Empty, fontTimesRoman, 10, true);
            form.AddField(tfDireccion);

            var rcObjeto = new Rectangle(93, 191, 727, 115);
            var tfObjeto = PdfFormField.CreateText(pdfDocument, rcObjeto, "objeto", string.Empty, fontTimesRoman, 10, true);
            form.AddField(tfObjeto);

            var rcFechaEmisionDia = new Rectangle(273, 168, 84, 20);
            var tfFechaEmisionDia = PdfFormField.CreateText(pdfDocument, rcFechaEmisionDia, "fechaEmisionDia", string.Empty, fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfFechaEmisionDia);

            var rcFechaEmisionMes = new Rectangle(373, 168, 232, 20);
            var tfFechaEmisionMes = PdfFormField.CreateText(pdfDocument, rcFechaEmisionMes, "fechaEmisionMes", string.Empty, fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfFechaEmisionMes);

            var rcFechaEmisionAnio = new Rectangle(620, 168, 200, 20);
            var tfFechaEmisionAnio = PdfFormField.CreateText(pdfDocument, rcFechaEmisionAnio, "fechaEmisionAnio", string.Empty, fontTimesRoman, 10).SetJustification(1);
            form.AddField(tfFechaEmisionAnio);

            var rcOperador = new Rectangle(128, 92, 380, 25);
            var tfOperador = PdfFormField.CreateText(pdfDocument, rcOperador, "operador", string.Empty, fontTimesRoman, 10);
            form.AddField(tfOperador);

            pdfDocument.Close();
            Console.WriteLine("Terminamos");
        }
        

          //metodo para generar el formato del sello, con los campos listos para escribir.
        private static void createFormSello()
        {
            //ruta donde se encuentra la imagen de fondo del sello
            const string patenteImage = @"C:\Users\gpisqui\Documents\BITBUCKET\libro_salarios\Diseño_sello\Sello_Dirección_Gral_de_Trbajo-03.jpg";            //ruta donde se guardaran los sellos .pdf generados
            const string pdfRoot = @"C:\Users\gpisqui\Documents\pdf\";
            //le asignamos nombre al .pdf
            var key = Guid.NewGuid();
            //var key = "Sello"; // Guid.NewGuid();
            var fileName = pdfRoot + key.ToString() + ".pdf";
            var pdfWriter = new PdfWriter(fileName);
            var pdfDocument = new PdfDocument(pdfWriter);
            var pageSize = PageSize.A4.Rotate();
            var document = new Document(pdfDocument, pageSize);
            var canvas = new PdfCanvas(pdfDocument.AddNewPage());
            canvas.AddImage(ImageDataFactory.CreateJpeg(new Uri(patenteImage)), pageSize, false);

            var form = PdfAcroForm.GetAcroForm(pdfDocument, true);
            var fontTimesRoman = PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN);
            var fontTimesRomanBold = PdfFontFactory.CreateFont(StandardFonts.TIMES_BOLD);
            //campo para NO DE REGISTRO
            var rctNo = new Rectangle(195, 379, 120, 26);
            var tfNo = PdfFormField.CreateText(pdfDocument, rctNo, "boleta", string.Empty, fontTimesRoman, 15).SetJustification(0);
            form.AddField(tfNo);
            //CAMPO PARA NIT DEL PATRONO
            var rcNit = new Rectangle(167, 338, 150, 26);
            var tfNit = PdfFormField.CreateText(pdfDocument, rcNit, "nit", string.Empty, fontTimesRoman, 15).SetJustification(0);
            form.AddField(tfNit);
            //CAMPO PARA NOMBRE DEL PATRONO
            var rctNom = new Rectangle(199, 301, 400, 26);
            var tfNom = PdfFormField.CreateText(pdfDocument, rctNom, "nombre", string.Empty, fontTimesRoman, 15).SetJustification(0);
            form.AddField(tfNom);
            //CAMPO PARA CANTIDAD DE FOLIOS
            var recFolios = new Rectangle(242, 261, 100, 26);
            var tfCantidad_folios = PdfFormField.CreateText(pdfDocument, recFolios, "cantidadFolios", string.Empty, fontTimesRoman, 15).SetJustification(0);
            form.AddField(tfCantidad_folios);
            //CAMPO PARA FECHA DE SOLICITUDAD
            var rectFechaSolicitud = new Rectangle(179, 226, 300, 26);
            var tfFechaSolicitud = PdfFormField.CreateText(pdfDocument, rectFechaSolicitud, "fechaSolicitud", string.Empty, fontTimesRoman, 15).SetJustification(0);
            form.AddField(tfFechaSolicitud);
            //CAMPO PARA FECHA DE AUTORIZACION 
            var rectFechaAutorizacion = new Rectangle(203, 192, 300, 26);
            var tfFechaAutorizacion = PdfFormField.CreateText(pdfDocument, rectFechaAutorizacion, "fechaAutorizacion", string.Empty, fontTimesRoman,15).SetJustification(0);
            form.AddField(tfFechaAutorizacion);
            
            pdfDocument.Close();

            Console.WriteLine("Terminamos");
        }
        
    }
}
