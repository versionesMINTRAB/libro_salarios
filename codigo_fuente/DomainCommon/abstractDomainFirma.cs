﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.batch;
using DataAccess.noSQL.cassandra.entities.document;
using DMLibroPatrono.batch;
using DMLibroPatrono.interfaces;
using EntityModel.noSQL.models.common;
using EntityModel.noSQL.models.document;
using iText.Kernel;
using iText.Kernel.Pdf;
using iText.Signatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DomainCommon
{
    public abstract class abstractDomainFirma<TKey, TModelPIN> : IFirma<TKey, TModelPIN>
    {
        protected readonly Func<IDocumentStoreConnection> storeConnection;

        protected abstractDomainFirma(Func<IDocumentStoreConnection> storeConnection)
        {
            this.storeConnection = storeConnection;
        }

        public abstract Task addPIN(TModelPIN pin);

        public async Task<bool> fileUploaded(Guid id)
        {
            var doc = new entityDocumentoDigital(storeConnection);
            return await doc.existsDocumento(id) > 0;
        }

        public abstract Task addDocumentToBatch(Guid batchID, TKey documento);
        public abstract Task<IEnumerable<TKey>> getPatentesByBatchID(Guid BatchID);

        public async Task addBatch(Guid id, IPAddress ip, int batchSize, string rootSigner, string user)
        {
            var entity = new entityLoteFirma(storeConnection);
            await entity.addFirma(new loteFirma
            { 
                id = id,
                ip = ip,
                user = user,
                startedAt = DateTimeOffset.UtcNow,
                batchSize = batchSize,
                rootSigner = rootSigner
            });
        }

        public async Task<modelLoteFirma> getJobFirma(Guid id)
        {
            var entity = new entityLoteFirma(storeConnection);
            return await entity.getFirma(id);
        }

        public async Task<int> getJobCounter(Guid id)
        {
            var data = new entityJobProgress(storeConnection);
            return await data.count(id);
        }

        public async Task incrementJobCounter(Guid id)
        {
            var entityJobProgress = new entityJobProgress(storeConnection);
            await entityJobProgress.increment(id);
        }

        public async Task uploadUnsignedFile(string user, int status, string descripcion, Guid id, Guid batchId, IPAddress ip, DateTimeOffset fecha)
        {
            var entityDocumento = new entityDocumentoFirmado(storeConnection);
            var entityPorFecha = new entityDocumentoPorFecha(storeConnection);
            var T1 = entityDocumento.addDocumento(new documentoFirmado
            {
                user = user,
                year = fecha.Year,
                id = id,
                ip = ip,
                fechaFirma = fecha,
                statusFirma = status,
                descripcion = descripcion,
                batchId = batchId
            });
            var T2 = entityPorFecha.addDocumento(new documentoPorFecha()
            {
                user = user,
                fechaFirma = fecha,
                id = id
            });
            await Task.WhenAll(T1, T2);
        }

        public async Task removeDocumento(Guid id)
        {
            var doc = new entityDocumentoDigital(storeConnection);
            await doc.removeDocument(id);
        }

        public async Task uploadSignedFile(string user, int status, string descripcion, Guid batchId, modelDocumentoFirmado documentoFirmado, TModelPIN pin)
        {
            var entityDocumento = new entityDocumentoFirmado(storeConnection);
            var entityPorFecha = new entityDocumentoPorFecha(storeConnection);
            var digitalDoc = new entityDocumentoDigital(storeConnection);
            var datosFirma = documentoFirmado.firmas.FirstOrDefault();
            var T0 = addPIN(pin);
            var T1 = entityDocumento.addDocumento(new documentoFirmado()
            {
                user = user,
                year = documentoFirmado.fechaFirma.Year,
                id = documentoFirmado.key,
                ip = documentoFirmado.ipOrigen,
                fechaFirma = documentoFirmado.fechaFirma,
                statusFirma = status,
                descripcion = descripcion,
                firmante = datosFirma != null ? datosFirma.firmante : string.Empty,
                razon = datosFirma != null ? datosFirma.razon : string.Empty,
                wholeDocumentSigned = datosFirma != null ? datosFirma.cubreTodoElDocumento : false,
                currentRevision = datosFirma != null ? datosFirma.revision : 0,
                totalRevisions = documentoFirmado.revisionesDocumento,
                isIntegral = datosFirma != null ? datosFirma.esIntegra : false,
                batchId = batchId
            });
            var T2 = entityPorFecha.addDocumento(new documentoPorFecha()
            {
                user = user,
                fechaFirma = documentoFirmado.fechaFirma,
                id = documentoFirmado.key
            });
            var T3 = digitalDoc.addDocumento(documentoFirmado.key, documentoFirmado.path);

            await Task.WhenAll(T0, T1, T2, T3);
        }

        public Tuple<int, IEnumerable<modelFirma>> verifySignatures(string path)
        {
            PdfDocument pdfDoc = new PdfDocument(new PdfReader(path));
            try
            {
                int totalRevisions = 0;
                SignatureUtil signUtil = new SignatureUtil(pdfDoc);
                var names = signUtil.GetSignatureNames();
                var result = new List<modelFirma>(names.Count());
                foreach (var name in names)
                {
                    var signature = signUtil.GetSignature(name);
                    PdfPKCS7 pkcs7 = signUtil.VerifySignature(name);
                    result.Add(new modelFirma()
                    {
                        nombreFirma = name,
                        firmante = signature.GetName(),
                        razon = signature.GetReason(),
                        cubreTodoElDocumento = signUtil.SignatureCoversWholeDocument(name),
                        revision = signUtil.GetRevision(name),
                        esIntegra = pkcs7.Verify()
                    });
                    totalRevisions = signUtil.GetTotalRevisions();
                }
                return new Tuple<int, IEnumerable<modelFirma>>(totalRevisions, result);
            }
            catch (PdfException ex)
            {
                Console.WriteLine("error (no hay firma?): " + ex.ToString());
                throw ex;
            }
            finally
            {
                pdfDoc.Close();
            }
        }

        
    }
}
