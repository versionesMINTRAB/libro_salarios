﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.document;
using DataAccess.noSQL.cassandra.entities.documentFlow;
using DataAccess.noSQL.cassandra.entities.libroPatrono;
using DMLibroPatrono;
using DMLibroPatrono.enums;
using DMLibroPatrono.interfaces;
using EntityModel.noSQL.models.documentFlow;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DomainCommon
{
    public abstract class abstractDomainDocument<TModel, TKey, TTokens, TModelPIN> : abstractDomainFirma<TKey,TModelPIN>,
        IPINManager<TModel, TModelPIN>,
        IWorkFlow<TModel>
        where TModel : ILibroPatrono<TKey>
        where TTokens : IToken
    {

        public abstractDomainDocument(Func<IDocumentStoreConnection> storeConnection):base(storeConnection)
        {
        }

        //public abstract Task<decimal> getBoleta();
        //public abstract Task<TModel> patenteByToken(Guid token);
        //public abstract Task<bool> setSimpleStatus(TKey id, int currentStatus, int newStatus);
        //public abstract Task<TModel> setStatus(TKey id, int currentStatus, int newStatus);
        //public abstract Task<bool> setStatus(TKey id, int newStatus);
        //public abstract Task<IEnumerable<TKey>> getByStatus(int status);
        //public abstract Task<IEnumerable<TTokens>> getTokens(TKey key);
        //public abstract Task addPIN(TModelPIN modelPIN);
        //public abstract Task<TModelPIN> getPIN(TModel modelPatente);
        //protected abstract Task<IEnumerable<TModelPIN>> getAllPINs();
        //public abstract Task<int> addDocumentsToSign(IPAddress ip, string user);
        public abstract Task signDocument(TModel document, Guid id, IPAddress ip, string user);
        public abstract Task<TModel> retrieveByID(signStatus status, Guid id, IPAddress ip, string user);
        public abstract Task<IEnumerable<TModel>> retrieveByStatus(signStatus status, byte[] page, IPAddress ip, string user);
        public abstract Task<IEnumerable<Tuple<Guid, TModel>>> documentosParaFirma(byte[] lote, int recordsPerRequest, IPAddress ip, string user);

        protected async Task<IEnumerable<documentByStatus>> _retrieveByStatus(signStatus status, byte[] page, IPAddress ip, string user)
        {
            var entity = new entityDocumentByStatus(storeConnection, ip, user);
            return await entity.retrieveByStatus(status, page);
        }

        protected async Task<documentByStatus> _retrieveByID(signStatus status, Guid id, IPAddress ip, string user)
        {
            var doc = new entityDocumentByStatus(storeConnection, ip, user);
            return await doc.retrieveByStatus(status, id);
        }

        /// <summary>
        /// Retrieves document raw data from the data store
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public abstract Task<TModel> documentByToken(Guid token);

        /// <summary>
        /// Retrieves the actual binary document
        /// </summary>
        /// <param name="pin"></param>
        /// <returns></returns>
       
        

        public abstract Task<TModelPIN> getPIN(TModel model);
        public abstract Task<byte[]> documentoFisico(TModelPIN pin);
        public abstract Task<Tuple<TModel, bool>> patenteByPIN(TModelPIN pin);
    }

}
