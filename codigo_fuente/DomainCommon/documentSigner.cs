﻿using commonModel.interfaces;
using DMLibroPatrono;
using DMLibroPatrono.interfaces;
using SMLibroPatrono;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DomainCommon
{
    public sealed class documentSigner<TModel> : IDocumentSigner<TModel> where TModel : class, new()
    {
        private readonly Func<IRPCClient<TModel, modelSignRemoteCall, modelSignResult>> rpcFactory;

        public documentSigner(Func<IRPCClient<TModel, modelSignRemoteCall, modelSignResult>> rpcFactory)
        {
            this.rpcFactory = rpcFactory;
        }

        public modelSignResult signDocuments(string archivo, string coordenadas, string pathRubrica, IPAddress clientIP)
        {
            var dataBag = new modelSignRemoteCall()
            {
                rootPath = archivo,
                coordenadas = coordenadas,
                pathRubrica = pathRubrica,
                clientIP = clientIP.ToString()
            };
            using (var rpc = rpcFactory())
            {
                return rpc.call(dataBag);
            }
        }
    }
}
