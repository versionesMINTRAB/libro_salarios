﻿using DataAccess.noSQL.cassandra.common;
using DMLibroPatrono.interfaces;
using iText.Barcodes.Qrcode;
using SMLibroPatrono.interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainCommon
{
    public abstract class abstractDomainDocumentMaker<TModel,TResult> : IDocumentMaker<TModel,TResult>
        where TResult : IDocumentoGenerado
    {
        protected readonly Func<IDocumentStoreConnection> storeConnection;
        protected readonly string urlDocumentReachedAt;
        protected readonly string uriTemplateForm;
        protected const string charset = "ISO-8859-1";
        protected readonly Dictionary<EncodeHintType, Object> hints;

        protected abstractDomainDocumentMaker(Func<IDocumentStoreConnection> storeConnection, string urlDocumentReachedAt, string uriTemplateForm)
        {
            this.storeConnection = storeConnection;
            this.urlDocumentReachedAt = urlDocumentReachedAt;
            this.uriTemplateForm = uriTemplateForm;
            hints = new Dictionary<EncodeHintType, Object>
            {
                { EncodeHintType.CHARACTER_SET, charset },
                { EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M }
            };
        }

        protected string whiteSpace(int spaces)
        {
            string tmp = string.Empty;
            for (int i = 0; i < spaces; i++)
            {
                tmp += " ";
            }
            return tmp;
        }

        protected string monthName(DateTimeOffset fecha)
        {
            switch (fecha.Month)
            {
                case 1: return "Enero";
                case 2: return "Febrero";
                case 3: return "Marzo";
                case 4: return "Abril";
                case 5: return "Mayo";
                case 6: return "Junio";
                case 7: return "Julio";
                case 8: return "Agosto";
                case 9: return "Septiembre";
                case 10: return "Octubre";
                case 11: return "Noviembre";
                case 12: return "Diciembre";
                default: return string.Empty;
            }
        }

        public abstract Task<TResult> makeDocument(TModel dataModel, string rootSigner, Guid batchId, Guid token);
    }
}
