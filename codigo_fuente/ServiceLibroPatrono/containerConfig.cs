﻿using Autofac;
using commonModel.interfaces;
using DataAccess.noSQL.cassandra.common;
using DMLibroPatrono;
using DMLibroPatrono.batch;
using DMLibroPatrono.interfaces;
using DomainCommon;
using DomainLibroPatrono;
using ServiceBroker.common;
using ServiceBroker.interfaces;
using ServiceLibroPatrono.parameters;
using ServiceLibroPatrono.subscribers;
using SMLibroPatrono;
using SMLibroPatrono.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceLibroPatrono
{
    public class containerConfig : Module
    {
        protected const string connCassandraLibroPatrono = "libroPatrono";
        protected const string connCassandraWorkFlow = "workflow";
        protected const string keyspacePatentes = "devMintrab";
        protected const string keyspaceWorkFlow = "devMintrab";
        protected readonly connectionStrings connectionStrings;
        protected readonly string eLibroPatronoURL;
        protected readonly string libroPatronoTemplateUri;

        public containerConfig(connectionStrings connectionStrings,string eLibroPatronoURL, string libroPatronoTemplateUri)
        {
            this.connectionStrings = connectionStrings;
            this.eLibroPatronoURL = eLibroPatronoURL;
            this.libroPatronoTemplateUri = libroPatronoTemplateUri;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            //--------------------Infrastructure----------------
            //Database
            builder.Register((c, p) => new clsCassandraConnection(connectionStrings.cassandraConn,
                                                                 keyspacePatentes,
                                                                 connectionStrings.cassandraUser,
                                                                 connectionStrings.cassandraPWD))
                  .SingleInstance()
                  .Named<IDocumentStoreConnection>(connCassandraLibroPatrono);
            builder.Register((c, p) => new clsCassandraConnection(connectionStrings.cassandraConn,
                                                                  keyspaceWorkFlow,
                                                                  connectionStrings.cassandraUser,
                                                                  connectionStrings.cassandraPWD))
                  .SingleInstance()
                  .Named<IDocumentStoreConnection>(connCassandraWorkFlow);

            //Service Broker
            builder.Register(c => new connectionRabbit(connectionStrings.rabbitMQConn))
                 .SingleInstance()
                 .OnActivated(e => e.Instance.connect())
                 .As<IServiceBrokerConnection>();

            //--------------------DomainApp Services----------------
            builder.Register(c => new domainLibroPatrono(c.ResolveNamed<Func<IDocumentStoreConnection>>(connCassandraLibroPatrono)))
                   .InstancePerDependency()
                   .As<IWorkFlow<modelLibroPatrono>>();

            builder.Register(c => new domainLibroPatrono(c.ResolveNamed<Func<IDocumentStoreConnection>>(connCassandraLibroPatrono)))
                   .InstancePerDependency()
                   .As<IPINManager<modelLibroPatrono, modelSinglePIN>>();

            builder.Register(c => new domainLibroPatronoDocumentMaker(c.ResolveNamed<Func<IDocumentStoreConnection>>(connCassandraLibroPatrono),
                                    eLibroPatronoURL, libroPatronoTemplateUri))
                   .SingleInstance()
                   .As<IDocumentMaker<modelLibroPatrono, modelDocumentoGenerado>>();

            builder.Register(c => new documentSigner<modelLibroPatrono>(
                c.Resolve<Func<IRPCClient<modelLibroPatrono, modelSignRemoteCall, modelSignResult>>>()))
                   .InstancePerDependency()
                   .As<IDocumentSigner<modelLibroPatrono>>();

            builder.Register(c => new domainLibroPatrono(c.ResolveNamed<Func<IDocumentStoreConnection>>(connCassandraLibroPatrono)))
                   .SingleInstance()
                   .As<IFirma<identificacionLibroPatrono, modelLibroPatrono>>();

            //--------------------Component Factories----------------
            builder.Register<Func<string, IDocumentStoreConnection>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return named => cc.ResolveNamed<IDocumentStoreConnection>(named);
            });

            builder.Register<Func<IFirma<identificacionLibroPatrono, modelLibroPatrono>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<IFirma<identificacionLibroPatrono, modelLibroPatrono>>;
            });
            
            builder.Register<Func<IPubUnsignedDoc<modelUnsignedDocument<modelLibroPatrono>>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<IPubUnsignedDoc<modelUnsignedDocument<modelLibroPatrono>>>;
            });
            
            builder.Register<Func<IDocumentMaker<modelLibroPatrono, modelDocumentoGenerado>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<IDocumentMaker<modelLibroPatrono, modelDocumentoGenerado>>;
            });


            builder.Register<Func<IPINManager<modelLibroPatrono, modelSinglePIN>>>(delegate (IComponentContext context)
             {
                 IComponentContext cc = context.Resolve<IComponentContext>();
                 return cc.Resolve<IPINManager<modelLibroPatrono, modelSinglePIN>>;
             });
            
            builder.Register<Func<IWorkFlow<modelLibroPatrono>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<IWorkFlow<modelLibroPatrono>>;
            });

            builder.Register<Func<IDocumentSigner<modelLibroPatrono>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<IDocumentSigner<modelLibroPatrono>>;
            });

            builder.Register<Func<IPubDocumentSigner<modelLibroPatrono>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<IPubDocumentSigner<modelLibroPatrono>>;
            });

            //--------------------Services => Subscribers----------------
            builder.Register(c => new subDataExtractor(c.Resolve<IServiceBrokerConnection>(),
                                                          c.Resolve<Func<IFirma<identificacionLibroPatrono, modelSinglePIN>>>(),
                                                          c.Resolve<Func<IPubUnsignedDoc<modelUnsignedDocument<modelLibroPatrono>>>>(),
                                                          c.Resolve<Func<IWorkFlow<modelLibroPatrono>>>()))
                   .InstancePerDependency()
                   .As<ISubDataExtractor<modelLibroPatrono>>();
            
            //RPCs
            //builder.Register(c => new RPC.rpcEmpresa(c.Resolve<IServiceBrokerConnection>()))
            //       .InstancePerDependency()
            //       .As<IRPCClient<modelPatenteEmpresa, modelSignRemoteCall, modelSignResult>>();
            //builder.Register(c => new RPC.rpcSociedad(c.Resolve<IServiceBrokerConnection>()))
            //       .InstancePerDependency()
            //       .As<IRPCClient<modelPatenteSociedad, modelSignRemoteCall, modelSignResult>>();

           

           

            builder.Register(c => new subPDFMaker(c.Resolve<IServiceBrokerConnection>(),
                                                     c.Resolve<Func<IDocumentMaker<modelPatenteEmpresa>>>(),
                                                     c.Resolve<Func<IPubDocumentSigner<modelPatenteEmpresa>>>()))
                   .InstancePerDependency()
                   .As<ISubDocumentMaker<modelPatenteEmpresa>>();
            builder.Register(c => new Subscribers.Sociedad.clsSubPDFMaker(c.Resolve<IServiceBrokerConnection>(),
                                                     c.Resolve<Func<IDocumentMaker<modelPatenteSociedad>>>(),
                                                     c.Resolve<Func<IPubDocumentSigner<modelPatenteSociedad>>>()))
                   .InstancePerDependency()
                   .As<ISubDocumentMaker<modelPatenteSociedad>>();

            builder.Register(c => new Subscribers.Empresa.clsSubDocumentSigner(
                c.Resolve<IServiceBrokerConnection>(),
                appSettings,
                c.Resolve<Func<IDocumentSigner<modelPatenteEmpresa>>>(),
                c.Resolve<Func<IFirma<identificacionDocumental, modelPINEmpresa>>>(),
                c.Resolve<Func<IDataRefresher<identificacionDocumental, modelPatenteEmpresa>>>(),
                c.Resolve<Func<IDomainPatente<modelPatenteEmpresa, modelPINEmpresa>>>(),
                c.Resolve<Func<IWorkFlow<modelPatenteEmpresa>>>()))
                   .InstancePerDependency()
                   .As<ISubDocumentSigner<modelPatenteEmpresa>>();

            builder.Register(c => new Subscribers.Sociedad.clsSubDocumentSigner(
                c.Resolve<IServiceBrokerConnection>(),
                appSettings,
                c.Resolve<Func<IDocumentSigner<modelPatenteSociedad>>>(),
                c.Resolve<Func<IFirma<identificacionPatenteSociedad, modelPINSociedad>>>(),
                c.Resolve<Func<IDataRefresher<identificacionPatenteSociedad, modelPatenteSociedad>>>(),
                c.Resolve<Func<IDomainPatente<modelPatenteSociedad, modelPINSociedad>>>(),
                c.Resolve<Func<IWorkFlow<modelPatenteSociedad>>>()))
                   .InstancePerDependency()
                   .As<ISubDocumentSigner<modelPatenteSociedad>>();

            //--------------------Services => Publishers----------------
            builder.Register(c => new Publishers.Empresa.clsPubDocumentSigner(c.Resolve<IServiceBrokerConnection>()))
                   .InstancePerDependency()
                   .As<IPubDocumentSigner<modelPatenteEmpresa>>();

            builder.Register(c => new Publishers.Sociedad.clsPubDocumentSigner(c.Resolve<IServiceBrokerConnection>()))
                   .InstancePerDependency()
                   .As<IPubDocumentSigner<modelPatenteSociedad>>();

            builder.Register(c => new Publishers.Empresa.clsPubDocumentMaker(c.Resolve<IServiceBrokerConnection>()))
                   .InstancePerDependency()
                   .As<IPubUnsignedDoc<modelUnsignedDocument<modelPatenteEmpresa>>>();

            builder.Register(c => new Publishers.Sociedad.clsPubDocumentMaker(c.Resolve<IServiceBrokerConnection>()))
                   .InstancePerDependency()
                   .As<IPubUnsignedDoc<modelUnsignedDocument<modelPatenteSociedad>>>();

            builder.Register(c => new Publishers.Empresa.clsPubStartSigner(
                c.Resolve<IServiceBrokerConnection>(),
                appSettings.rootPDF,
                c.Resolve<Func<IFirma<identificacionDocumental, modelPINEmpresa>>>(),
                c.Resolve<Func<IWorkFlow<modelPatenteEmpresa>>>()))
                   .InstancePerDependency()
                   .As<IPubStartSigner<identificacionDocumental>>();

            builder.Register(c => new Publishers.Sociedad.clsPubStartSigner(
                c.Resolve<IServiceBrokerConnection>(),
                appSettings.rootPDF,
                c.Resolve<Func<IFirma<identificacionPatenteSociedad, modelPINSociedad>>>(),
                c.Resolve<Func<IWorkFlow<modelPatenteSociedad>>>()))
                   .InstancePerDependency()
                   .As<IPubStartSigner<identificacionPatenteSociedad>>();

            //--------------------Service Definitions----------------
            builder.Register(c => new defPatenteElectronica(c.Resolve<IServiceBrokerConnection>()))
                   .SingleInstance()
                   .Named<IServiceBroker>("defPatenteElectronica");

            //--------------------Service Factories----------------
            builder.Register<Func<IPubStartSigner<identificacionDocumental>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<IPubStartSigner<identificacionDocumental>>;
            });
            builder.Register<Func<IPubStartSigner<identificacionPatenteSociedad>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<IPubStartSigner<identificacionPatenteSociedad>>;
            });
            builder.Register<Func<ISubDataExtractor<modelPatenteEmpresa>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<ISubDataExtractor<modelPatenteEmpresa>>;
            });
            builder.Register<Func<ISubDataExtractor<modelPatenteSociedad>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<ISubDataExtractor<modelPatenteSociedad>>;
            });
            builder.Register<Func<ISubDocumentMaker<modelPatenteEmpresa>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<ISubDocumentMaker<modelPatenteEmpresa>>;
            });
            builder.Register<Func<ISubDocumentMaker<modelPatenteSociedad>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<ISubDocumentMaker<modelPatenteSociedad>>;
            });
            builder.Register<Func<ISubDocumentSigner<modelPatenteSociedad>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<ISubDocumentSigner<modelPatenteSociedad>>;
            });
            builder.Register<Func<ISubDocumentSigner<modelPatenteEmpresa>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<ISubDocumentSigner<modelPatenteEmpresa>>;
            });
            builder.Register<Func<ISubGarbageCollector<modelPatenteEmpresa>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<ISubGarbageCollector<modelPatenteEmpresa>>;
            });
            builder.Register<Func<ISubGarbageCollector<modelPatenteSociedad>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<ISubGarbageCollector<modelPatenteSociedad>>;
            });
            builder.Register<Func<IRPCClient<modelPatenteEmpresa, modelSignRemoteCall, modelSignResult>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<IRPCClient<modelPatenteEmpresa, modelSignRemoteCall, modelSignResult>>;
            });
            builder.Register<Func<IRPCClient<modelPatenteSociedad, modelSignRemoteCall, modelSignResult>>>(delegate (IComponentContext context)
            {
                IComponentContext cc = context.Resolve<IComponentContext>();
                return cc.Resolve<IRPCClient<modelPatenteSociedad, modelSignRemoteCall, modelSignResult>>;
            });

            //--------------------Service Implementations----------------
            builder.Register(c => new cfgPatenteElectronica(appSettings.concurrentSubscribers,
                                                            c.Resolve<Func<ISubDataExtractor<modelPatenteEmpresa>>>(),
                                                            c.Resolve<Func<ISubDataExtractor<modelPatenteSociedad>>>(),
                                                            c.Resolve<Func<ISubDocumentMaker<modelPatenteEmpresa>>>(),
                                                            c.Resolve<Func<ISubDocumentMaker<modelPatenteSociedad>>>(),
                                                            c.Resolve<Func<ISubDocumentSigner<modelPatenteEmpresa>>>(),
                                                            c.Resolve<Func<ISubDocumentSigner<modelPatenteSociedad>>>(),
                                                            c.Resolve<Func<ISubGarbageCollector<modelPatenteEmpresa>>>(),
                                                            c.Resolve<Func<ISubGarbageCollector<modelPatenteSociedad>>>(),
                                                            c.ResolveNamed<IServiceBroker>("defPatenteElectronica")))
                   .SingleInstance()
                   .As<IServicesHost>();
        }
    }
}
