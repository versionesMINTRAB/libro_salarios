﻿using DMLibroPatrono;
using DMLibroPatrono.interfaces;
using ServiceBroker.interfaces;
using ServiceLibroPatrono.common;
using SMLibroPatrono;
using SMLibroPatrono.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceLibroPatrono.subscribers
{
    public sealed class subDataExtractor : abstractSubDataExtractor<modelLibroPatrono, identificacionLibroPatrono, modelSinglePIN>
    {
        public subDataExtractor(
            IServiceBrokerConnection conn,
            Func<IFirma<identificacionLibroPatrono, modelSinglePIN>> storeFactory,
            Func<IPubUnsignedDoc<modelUnsignedDocument<modelLibroPatrono>>> documentPubFactory,
            Func<IWorkFlow<modelLibroPatrono>> workflowFactory) : base(conn, storeFactory, documentPubFactory, workflowFactory)
        {
        }

        protected override string serviceName => "ServiceLibroPatrono.subscribers.subDataExtractor";
        protected override string queueName => constants.libroPatrono.queues.qDataRetrieval;
        protected override string routingKey => constants.libroPatrono.events.startSigner;
    }
}
