﻿using DMLibroPatrono;
using DMLibroPatrono.batch;
using ServiceBroker.interfaces;
using ServiceLibroPatrono.common;
using SMLibroPatrono.interfaces;
using System;

namespace ServiceLibroPatrono.subscribers
{
    public sealed class subPDFMaker : abstractSubPDFMaker<modelLibroPatrono, modelDocumentoGenerado>
    {
        public subPDFMaker(IServiceBrokerConnection conn, 
            Func<IDocumentMaker<modelLibroPatrono, modelDocumentoGenerado>> documentMaker, 
            Func<IPubDocumentSigner<modelLibroPatrono>> pubDocumentSigner) : base(conn, documentMaker, pubDocumentSigner)
        {
        }

        protected override string serviceName => "ServiceLibroPatrono.subscribers.subPDFMaker";

        protected override string queueName => constants.libroPatrono.queues.qPDFMaker;

        protected override string routingKey => constants.libroPatrono.events.makePDF;
    }
}
