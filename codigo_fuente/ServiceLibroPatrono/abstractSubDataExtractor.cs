﻿using DMLibroPatrono.enums;
using DMLibroPatrono.interfaces;
using ServiceBroker.common;
using ServiceBroker.interfaces;
using SMLibroPatrono;
using SMLibroPatrono.interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibroPatrono
{
    public abstract class abstractSubDataExtractor<TModel, TKey, TModelPIN> : abstractSubscriberAsync<modelUnsignedDocument<TKey>>, ISubDataExtractor<TModel>
        where TModel: ILibroPatrono<TKey>, IDocumentoGenerado
    {
        private readonly Func<IPubUnsignedDoc<modelUnsignedDocument<TModel>>> documentPubFactory;
        private readonly Func<IFirma<TKey, TModelPIN>> storeFactory;
        private readonly Func<IWorkFlow<TModel>> workflowFactory;

        public abstractSubDataExtractor(IServiceBrokerConnection conn,
                                   Func<IFirma<TKey, TModelPIN>> storeFactory,
                                   Func<IPubUnsignedDoc<modelUnsignedDocument<TModel>>> documentPubFactory,
                                   Func<IWorkFlow<TModel>> workflowFactory) : base(conn)
        {
            this.documentPubFactory = documentPubFactory;
            this.workflowFactory = workflowFactory;
            this.storeFactory = storeFactory;
        }

        protected override async Task onMessage(modelUnsignedDocument<TKey> Message)
        {
            var wf = workflowFactory();
            var store = storeFactory();
            var documento = await wf.retrieveByID(signStatus.Unsigned, Message.token, Message.ip, Message.user);
            if (documento != null)
            {
                await store.addDocumentToBatch(Message.contexto, documento.id);
                var pub = documentPubFactory();
                pub.makeDocument(new modelUnsignedDocument<TModel>()
                {
                    contexto = Message.contexto,
                    document = documento,
                    rootSigner = Message.rootSigner,
                    token = Message.token,
                    ip = Message.ip
                });
            }
        }
    }
}
