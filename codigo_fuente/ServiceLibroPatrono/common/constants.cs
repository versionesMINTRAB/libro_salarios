﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceLibroPatrono.common
{
    internal sealed class constants
    {
        public const string libroPatronoExchange = "libroPatrono";
        public const string mimeTypeAppJson = "application/json";

        public sealed class libroPatrono
        {
            public sealed class queues
            {
                public const string deadLetterQueue = "libroPatrono.dlq";
                public const string qDataRetrieval = "libroPatrono.dataretrieval";
                public const string qPDFMaker = "libroPatrono.pdfMaker";
                public const string qPDFSigner = "libroPatrono.pdfSigner";
                public const string qPDFGC = "libroPatrono.pdfGC";
            }
            public sealed class events
            {
                public const string startSigner = "mintrab.libroPatrono.startSigner"; //Crea un nuevo root y lo publica junto con el lote de llaves de patentes a generar
                public const string retrieveData = "mintrab.libroPatrono.retrieveData"; //Extrae datos de la BDD de una patente y los publica
                public const string makePDF = "mintrab.libroPatrono.makePDF"; //Crea un PDF, lo almacena en el store y publica su ubicación
                public const string signPDF = "mintrab.libroPatrono.signPDF"; //Firma el PDF, lo almacena en el store y publica su ubicación
                public const string dealWithZombies = "mintrab.libroPatrono.zombieManagement"; //Reportar casos con problema
                public const string remoteCallSignPDF = "mintrab.libroPatrono.remoteSignature"; //RPC para llamada a WS desde .Net Framework
            }
        }
    }
}
