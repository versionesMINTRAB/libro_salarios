﻿using commonModel.interfaces;
using DMLibroPatrono;
using ServiceBroker.interfaces;
using ServiceLibroPatrono.parameters;
using SMLibroPatrono.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceLibroPatrono.common
{
    public sealed class cfgLibroPatrono : IServicesHost
    {
        private readonly concurrentSubscribers concurrentSubscribers;
        private readonly IServiceBroker serviceBroker;
        private readonly Func<ISubDataExtractor<modelLibroPatrono>> dataExtractorFactoryLibroPatrono;
        private readonly Func<ISubDocumentMaker<modelLibroPatrono>> documentMakerFactoryLibroPatrono;
        private readonly Func<ISubDocumentSigner<modelLibroPatrono>> documentSignerFactoryLibroPatrono;
        private List<ISubDataExtractor<modelLibroPatrono>> dataExtractorsLibroPatrono;
        private List<ISubDocumentMaker<modelLibroPatrono>> documentMakersLibroPatrono;
        private ISubDocumentSigner<modelLibroPatrono> documentSignerLibroPatrono;

        public cfgLibroPatrono(concurrentSubscribers concurrentSubscribers, 
                               IServiceBroker serviceBroker, 
                               Func<ISubDataExtractor<modelLibroPatrono>> dataExtractorFactoryLibroPatrono, 
                               Func<ISubDocumentMaker<modelLibroPatrono>> documentMakerFactoryLibroPatrono, 
                               Func<ISubDocumentSigner<modelLibroPatrono>> documentSignerFactoryLibroPatrono, 
                               List<ISubDataExtractor<modelLibroPatrono>> dataExtractorsLibroPatrono, 
                               List<ISubDocumentMaker<modelLibroPatrono>> documentMakersLibroPatrono, 
                               ISubDocumentSigner<modelLibroPatrono> documentSignerLibroPatrono)
        {
            this.concurrentSubscribers = concurrentSubscribers;
            this.serviceBroker = serviceBroker;
            this.dataExtractorFactoryLibroPatrono = dataExtractorFactoryLibroPatrono;
            this.documentMakerFactoryLibroPatrono = documentMakerFactoryLibroPatrono;
            this.documentSignerFactoryLibroPatrono = documentSignerFactoryLibroPatrono;
            this.dataExtractorsLibroPatrono = dataExtractorsLibroPatrono;
            this.documentMakersLibroPatrono = documentMakersLibroPatrono;
            this.documentSignerLibroPatrono = documentSignerLibroPatrono;
            initialize();
        }

        private void initialize()
        {
            int i;
            dataExtractorsLibroPatrono = new List<ISubDataExtractor<modelLibroPatrono>>(concurrentSubscribers.libroPatrono.dataExtractor);
            documentMakersLibroPatrono = new List<ISubDocumentMaker<modelLibroPatrono>>(concurrentSubscribers.libroPatrono.pDFMaker);

            for (i = 0; i < concurrentSubscribers.libroPatrono.dataExtractor; i++)
            {
                dataExtractorsLibroPatrono.Add(dataExtractorFactoryLibroPatrono());
            }
            for (i = 0; i < concurrentSubscribers.libroPatrono.pDFMaker; i++)
            {
                documentMakersLibroPatrono.Add(documentMakerFactoryLibroPatrono());
            }
            documentSignerLibroPatrono = documentSignerFactoryLibroPatrono();
        }

        public void startSubscribers()
        {
            serviceBroker.runDefinition();
            dataExtractorsLibroPatrono.ForEach(x => x.startListener());
            documentMakersLibroPatrono.ForEach(x => x.startListener());
            documentSignerLibroPatrono.startListener();
        }

        public void stopSubscribers()
        {
            dataExtractorsLibroPatrono.ForEach(x => x.closeListener());
            documentMakersLibroPatrono.ForEach(x => x.closeListener());
            documentSignerLibroPatrono.closeListener();
        }
    }
}
