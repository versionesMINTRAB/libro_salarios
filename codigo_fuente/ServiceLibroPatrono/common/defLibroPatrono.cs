﻿using RabbitMQ.Client;
using ServiceBroker.common;
using ServiceBroker.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceLibroPatrono.common
{
    public sealed class defLibroPatrono : infrastructure, IServiceBroker
    {
        public defLibroPatrono(IServiceBrokerConnection Conn) : base(Conn)
        {
        }

        public void runDefinition()
        {
            using (var channel = myConn.connection.CreateModel())
            {
                serviceFallBack(channel, constants.libroPatrono.queues.deadLetterQueue);
                serviceDefinition(channel);
            }
        }

        protected override void serviceDefinition(IModel channel)
        {
            channel.ExchangeDeclare(constants.libroPatronoExchange, ExchangeType.Topic, true);
        }
    }
}
