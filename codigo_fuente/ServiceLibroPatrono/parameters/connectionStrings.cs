﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceLibroPatrono.parameters
{
    public sealed class connectionStrings
    {
        public string rabbitMQConn { get; set; }
        public string cassandraConn { get; set; }
        public string cassandraUser { get; set; }
        public string cassandraPWD { get; set; }
    }
}
