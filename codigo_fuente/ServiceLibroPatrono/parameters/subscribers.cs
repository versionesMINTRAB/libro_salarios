﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceLibroPatrono.parameters
{
    public sealed class subscribers
    {
        public int dataExtractor { get; set; }
        public int pDFMaker { get; set; }
    }
}
