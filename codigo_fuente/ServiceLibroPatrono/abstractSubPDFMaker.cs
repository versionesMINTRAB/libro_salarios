﻿using DMLibroPatrono.interfaces;
using ServiceBroker.common;
using ServiceBroker.interfaces;
using ServiceLibroPatrono.common;
using SMLibroPatrono;
using SMLibroPatrono.common;
using SMLibroPatrono.interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibroPatrono
{
    public abstract class abstractSubPDFMaker<TModel, TResult> : abstractSubscriberAsync<modelUnsignedDocument<TModel>>, ISubDocumentMaker<TModel>
        where TResult : IDocumentoGenerado
    {
        private readonly Func<IDocumentMaker<TModel, TResult>> documentMaker;
        private readonly Func<IPubDocumentSigner<TModel>> pubDocumentSigner;

        public abstractSubPDFMaker(
            IServiceBrokerConnection conn,
            Func<IDocumentMaker<TModel, TResult>> documentMaker,
            Func<IPubDocumentSigner<TModel>> pubDocumentSigner) : base(conn)
        {
            this.documentMaker = documentMaker;
            this.pubDocumentSigner = pubDocumentSigner;
        }

        protected override sealed async Task onMessage(modelUnsignedDocument<TModel> Message)
        {
            var docMaker = documentMaker();
            var pubDocument = pubDocumentSigner();

            //pubDocument.signDocument(new modelUnsignedFile()
            //{
            //    contexto = Message.contexto,
            //    ip = Message.ip,
            //    user = Message.user,
            //    documentoGenerado = Message.document != null ? await docMaker.makeDocument(Message.document, Message.rootSigner, Message.contexto, Message.token) : null
            //});

            var hola = await docMaker.makeDocument(Message.document, Message.rootSigner, Message.contexto, Message.token);
            pubDocument.signDocument(new modelUnsignedFile()
            {
                contexto = Message.contexto,
                ip = Message.ip,
                user = Message.user,
                documentoGenerado = hola
            });
        }
    }
}
