﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commonModel.interfaces
{
    public interface IServicesHost
    {
        void startSubscribers();
        void stopSubscribers();
    }
}
