﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commonModel.interfaces
{
    public interface ISubscriber
    {
        void startListener();
        void closeListener();
    }
}
