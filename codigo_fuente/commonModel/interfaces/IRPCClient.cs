﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commonModel.interfaces
{
    public interface IRPCClient<TModel, TInput, TResult> : IDisposable
        where TModel : class, new()
        where TInput : class, new()
        where TResult : class, new()
    {
        TResult call(TInput message);
    }
}
