﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace commonModel
{
    public class ipAddressConverter : JsonConverter<IPAddress>
    {
        public override IPAddress ReadJson(JsonReader reader, Type objectType, IPAddress existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (reader.Value != null)
            {
                byte[] bytes = Encoding.UTF8.GetBytes((string)reader.Value);
                return hasExistingValue ? existingValue : new IPAddress(bytes);
            }
            return null;
        }

        public override void WriteJson(JsonWriter writer, IPAddress value, JsonSerializer serializer)
        {
            var tmp = value.GetAddressBytes();
            writer.WriteValue(Encoding.UTF8.GetString(tmp));
        }
    }
}
