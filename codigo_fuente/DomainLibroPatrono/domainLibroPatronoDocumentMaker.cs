﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.libroPatrono;
using DMLibroPatrono;
using DMLibroPatrono.batch;
using DomainCommon;
using iText.Barcodes;
using iText.Forms;
using iText.IO.Font.Constants;
using iText.IO.Image;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DomainLibroPatrono
{
    public sealed class domainLibroPatronoDocumentMaker : abstractDomainDocumentMaker<modelLibroPatrono, modelDocumentoGenerado>
    {
        public domainLibroPatronoDocumentMaker(Func<IDocumentStoreConnection> storeConnection, string urlDocumentReachedAt, string uriTemplateForm) : base(storeConnection, urlDocumentReachedAt, uriTemplateForm)
        {
        }

        public override async Task<modelDocumentoGenerado> makeDocument(modelLibroPatrono dataModel, string rootSigner, Guid batchId, Guid token)
        {
            var key = token;
            var fileName = rootSigner + "\\" + key.ToString() + ".pdf";
            //Texto inhabilitado hasta tener una plantilla disponible para darle formato
            //using (var patenteForm = new MemoryStream(File.ReadAllBytes(uriTemplateForm)))
            //{
            //    var pdfDocument = new PdfDocument(new PdfReader(patenteForm), new PdfWriter(fileName));
            //    var form = PdfAcroForm.GetAcroForm(pdfDocument, false);
            //    form.SetGenerateAppearance(true);
            //    var fontTimesRoman = PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN);
            //    form.GetField("boleta").SetValue("No: " + (int)decimal.Parse(dataModel.boleta.ToString()));
            //    form.GetField("titular").SetValue("Titular: " + dataModel.titular.serie + "-" + dataModel.titular.numero.ToString());
            //    form.GetField("nombre").SetValue(dataModel.nombre);
            //    form.GetField("registro").SetValue(dataModel.id.registro);
            //    form.GetField("folio").SetValue(dataModel.id.folio.ToString());
            //    form.GetField("libro").SetValue(dataModel.id.libro.ToString());
            //    form.GetField("expediente").SetValue(dataModel.expediente);
            //    var maskDescripcion = dataModel.categoria != null && dataModel.categoria.descripcion != null ? dataModel.categoria.descripcion : string.Empty;
            //    form.GetField("categoria").SetValue(maskDescripcion);
            //    form.GetField("direccion").SetValue("\n" + dataModel.direccionComercial);
            //    var maskObjeto = dataModel.objeto ?? string.Empty;
            //    maskObjeto += " ---ÚLTIMA LINEA---";
            //    if (maskObjeto.Length > 1231)
            //    {
            //        maskObjeto = maskObjeto.Substring(0, 1199) + " ... ver objeto completo en QR";
            //    }

            //    form.GetField("objeto").SetValue("\n" + maskObjeto);
            //    form.GetField("fechaInscripcionDia").SetValue(dataModel.fechaInscripcion.Value.Day.ToString());
            //    form.GetField("fechaInscripcionMes").SetValue(monthName(dataModel.fechaInscripcion.Value));
            //    form.GetField("fechaInscripcionAnio").SetValue(dataModel.fechaInscripcion.Value.Year.ToString());
            //    if (dataModel.propietario.Any())
            //    {
            //        string nombres, maskCui, maskNac, maskExtendidaEnMunicipio, maskExtendidaEnDepartamento, propDireccion, idLabel;
            //        if (dataModel.propietario.Count() > 1)
            //        {
            //            nombres = "Ver copropietarios al dorso";
            //            maskCui = string.Empty;
            //            maskNac = string.Empty;
            //            maskExtendidaEnMunicipio = string.Empty;
            //            maskExtendidaEnDepartamento = string.Empty;
            //            propDireccion = string.Empty;
            //            idLabel = string.Empty;
            //        }
            //        else
            //        {
            //            nombres = dataModel.propietario.First().nombre;
            //            var cui = dataModel.claseEstablecimiento.Equals(constants.claseEstablecimiento.individual) ? dataModel.propietario.First().identificacion : "***********     ***********";
            //            idLabel = !string.IsNullOrEmpty(dataModel.propietario.First().identificacionLabel) ? dataModel.propietario.First().identificacionLabel : string.Empty;
            //            var extendidaEnMunicipio = dataModel.claseEstablecimiento.Equals(constants.claseEstablecimiento.individual) ? dataModel.propietario.First().extendidaEn.municipio : "***********";
            //            var extendidaEnDepartamento = dataModel.claseEstablecimiento.Equals(constants.claseEstablecimiento.individual) ? dataModel.propietario.First().extendidaEn.departamento : "***********";
            //            maskCui = !string.IsNullOrEmpty(cui) ? cui : string.Empty;
            //            maskNac = !string.IsNullOrEmpty(dataModel.propietario.First().nacionalidad) ? dataModel.propietario.First().nacionalidad : string.Empty;
            //            maskExtendidaEnMunicipio = !string.IsNullOrEmpty(extendidaEnMunicipio) ? extendidaEnMunicipio : string.Empty;
            //            maskExtendidaEnDepartamento = !string.IsNullOrEmpty(extendidaEnDepartamento) ? extendidaEnDepartamento : string.Empty;
            //            propDireccion = !string.IsNullOrEmpty(dataModel.propietario.First().direccion) ? dataModel.propietario.First().direccion : "(no disponible)";
            //        }
            //        form.GetField("propietarioNombre").SetValue("\n" + nombres);
            //        form.GetField("propietarioNacionalidad").SetValue(maskNac);
            //        form.GetField("propietarioIdentificacionLabel").SetValue(idLabel);
            //        form.GetField("propietarioIdentificacion").SetValue(maskCui);
            //        form.GetField("propietarioExtendidaEnMunicipio").SetValue(maskExtendidaEnMunicipio);
            //        form.GetField("propietarioExtendidaEnMDepartamento").SetValue(maskExtendidaEnDepartamento);
            //        form.GetField("propietarioDireccion").SetValue("\n" + propDireccion);
            //    }
            //    var maskClaseEstablecimiento = dataModel.claseEstablecimiento ?? string.Empty;
            //    form.GetField("claseEstablecimiento").SetValue(maskClaseEstablecimiento);
            //    var repLegal = !string.IsNullOrEmpty(dataModel.representanteLegal) ? dataModel.representanteLegal : string.Empty;
            //    form.GetField("representanteLegal").SetValue(repLegal);
            //    form.GetField("fechaEmisionDia").SetValue(DateTime.Now.Day.ToString());
            //    form.GetField("fechaEmisionMes").SetValue(monthName(DateTime.Now));
            //    form.GetField("fechaEmisionAnio").SetValue(DateTime.Now.Year.ToString());
            //    var maskOperador = dataModel.operador ?? string.Empty;
            //    form.GetField("operador").SetValue(maskOperador);
            //    form.FlattenFields();

            //    //Rubrica operador
            //    var document = new Document(pdfDocument);
            //    if (!string.IsNullOrEmpty(dataModel.pathRubricaOperador))
            //    {
            //        var rubricaOperador = await File.ReadAllBytesAsync(dataModel.pathRubricaOperador);
            //        Image img = new Image(ImageDataFactory.CreatePng(rubricaOperador));
            //        img.Scale(0.8F, 0.8F);
            //        img.SetFixedPosition(220, 20);
            //        document.Add(img);
            //    }
            //    //QR
            //    BarcodeQRCode barcodeQRCode = new BarcodeQRCode(urlDocumentReachedAt + key.ToString(), hints);
            //    Image codeQrImage = new Image(barcodeQRCode.CreateFormXObject(pdfDocument), 690F, 30F);
            //    codeQrImage.SetWidth(120F);
            //    document.Add(codeQrImage);

            //    //For more than 1 owner, add a new page with the list of them
            //    if (dataModel.propietario.Count() > 1)
            //    {
            //        var tableProps = new Table(6);
            //        tableProps.SetTextAlignment(iText.Layout.Properties.TextAlignment.RIGHT);

            //        //Titulo:
            //        tableProps.AddCell(new Cell(2, 1)
            //                    .Add(new Paragraph("Nombre"))
            //                    .SetBackgroundColor(iText.Kernel.Colors.ColorConstants.LIGHT_GRAY)
            //                    .SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER));
            //        tableProps.AddCell(new Cell(2, 1)
            //                    .Add(new Paragraph("Nacionalidad"))
            //                    .SetBackgroundColor(iText.Kernel.Colors.ColorConstants.LIGHT_GRAY)
            //                    .SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER));
            //        tableProps.AddCell(new Cell(2, 1)
            //                    .Add(new Paragraph("Documento Personal de Identificación"))
            //                    .SetBackgroundColor(iText.Kernel.Colors.ColorConstants.LIGHT_GRAY)
            //                    .SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER));
            //        tableProps.AddCell(new Cell(1, 2)
            //                    .Add(new Paragraph("Extendida En"))
            //                    .SetBackgroundColor(iText.Kernel.Colors.ColorConstants.LIGHT_GRAY)
            //                    .SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER));
            //        tableProps.AddCell(new Cell(2, 1)
            //                    .Add(new Paragraph("Dirección"))
            //                    .SetBackgroundColor(iText.Kernel.Colors.ColorConstants.LIGHT_GRAY)
            //                    .SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER));

            //        tableProps.AddCell(new Cell()
            //                    .Add(new Paragraph("Municipio"))
            //                    .SetBackgroundColor(iText.Kernel.Colors.ColorConstants.LIGHT_GRAY)
            //                    .SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER));
            //        tableProps.AddCell(new Cell()
            //                    .Add(new Paragraph("Departamento"))
            //                    .SetBackgroundColor(iText.Kernel.Colors.ColorConstants.LIGHT_GRAY)
            //                    .SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER));

            //        //Lista de propietarios
            //        foreach (var propietario in dataModel.propietario)
            //        {
            //            tableProps.AddCell(new Cell()
            //                    .Add(new Paragraph(propietario.nombre)));
            //            tableProps.AddCell(new Cell()
            //                    .Add(new Paragraph(propietario.nacionalidad ?? string.Empty)));
            //            tableProps.AddCell(new Cell()
            //                    .Add(new Paragraph(propietario.identificacion ?? string.Empty)));
            //            tableProps.AddCell(new Cell()
            //                    .Add(new Paragraph(propietario.extendidaEn.municipio ?? string.Empty)));
            //            tableProps.AddCell(new Cell()
            //                    .Add(new Paragraph(propietario.extendidaEn.departamento ?? string.Empty)));
            //            tableProps.AddCell(new Cell()
            //                    .Add(new Paragraph(propietario.direccion ?? string.Empty)));
            //        }
            //        document.Add(new AreaBreak());
            //        document.Add(tableProps);
            //    }
            //    document.Close();
            //}
            //var entity = new entityTokenLibroPatrono(storeConnection);
            //var rawEntity = new entityRawLibroPatrono(storeConnection);
            //await rawEntity.addModel(key, dataModel);
            //await entity.addDocumento(new tokenLibroPatrono()
            //{
            //    batchId = batchId,
            //    fecha = dataModel.id.fecha,
            //    nit = dataModel.id.nit,
            //    id = key
            //});
            return await Task.FromResult(new modelDocumentoGenerado()
            {
                key = key,
                path = fileName
            });
        }
    }
}
