﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.batch;
using DataAccess.noSQL.cassandra.entities.document;
using DataAccess.noSQL.cassandra.entities.documentFlow;
using DataAccess.noSQL.cassandra.entities.libroPatrono;
using DMLibroPatrono;
using DMLibroPatrono.batch;
using DMLibroPatrono.enums;
using DomainCommon;
using EntityModel.noSQL.models.libroPatrono;
using iText.Barcodes;
using iText.Forms;
using iText.IO.Font.Constants;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DomainLibroPatrono
{
    public sealed class domainLibroPatrono : 
        abstractDomainDocument<modelLibroPatrono, identificacionLibroPatrono, modelTokenLibroPatrono, modelSinglePIN>
    {

        public domainLibroPatrono(Func<IDocumentStoreConnection> storeConnection) : base(storeConnection)
        {
        }

        /// <summary>
        /// Agrega un documento al lote de documentos a ser firmados
        /// </summary>
        /// <param name="batchID"></param>
        /// <param name="documento"></param>
        /// <returns></returns>
        public override async Task addDocumentToBatch(Guid batchID, identificacionLibroPatrono documento)
        {
            var entity = new entityLoteDocumentos(storeConnection);
            await entity.addDocumento(batchID, documento);
        }

        /// <summary>
        /// Agrega el PIN generado para un libro, referenciando la llave del libro con el PIN
        /// </summary>
        /// <param name="pin"></param>
        /// <returns></returns>
        public override async Task addPIN(modelSinglePIN pin)
        {
            if (pin != null)
            {
                var entity = new entityLookupByPIN(storeConnection);
                await entity.addPIN(pin);
            }
        }

        public override async Task<modelLibroPatrono> documentByToken(Guid token)
        {
            var rawEntity = new entityRawLibroPatrono(storeConnection);
            return await rawEntity.getModelAsJSON(token);
        }

        public override async Task<byte[]> documentoFisico(modelSinglePIN pin)
        {
            var docEntity = new entityDocumentoDigital(storeConnection);
            var lookupEntity = new entityLookupByPIN(storeConnection);
            var res = await lookupEntity.lookup(pin);
            if (res.Item3 && res.Item2.HasValue)
            {
                var downloadCounter = new entityDocumentoDescargas(storeConnection);
                var count = await downloadCounter.increment(res.Item2.Value);
                var doc = new entityDocumentoDigital(storeConnection);
                var r = await doc.getDocumento(res.Item2.Value);
                if (r != null && r.file != null)
                {
                    return r.file;
                }
            }
            return null;
        }

        public override async Task<IEnumerable<Tuple<Guid, modelLibroPatrono>>> documentosParaFirma(byte[] lote, int recordsPerRequest, IPAddress ip, string user)
        {
            var data = await _retrieveByStatus(signStatus.Unsigned, lote, ip, user);
            return data.Where(x => x.document != null).Select(x => new Tuple<Guid, modelLibroPatrono>(x.tkn, x.document));
        }

        public override async Task<IEnumerable<identificacionLibroPatrono>> getPatentesByBatchID(Guid BatchID)
        {
            var entity = new entityLoteDocumentos(storeConnection);
            return await entity.documentosByBatchId(BatchID);
        }

        public override async Task<modelSinglePIN> getPIN(modelLibroPatrono model)
        {
            int lengthOfPassword = 8;
            string pass = BitConverter.ToString(new SHA512CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(model.libroPatrono.nit))).Replace("-", string.Empty);
            return await Task.FromResult(new modelSinglePIN()
            {
                libroPatrono = model.id,
                pin = pass.Substring(0, lengthOfPassword)
            });
        }

        public override Task<Tuple<modelLibroPatrono, bool>> patenteByPIN(modelSinglePIN pin)
        {
            throw new NotImplementedException();
        }

        public override async Task<modelLibroPatrono> retrieveByID(signStatus status, Guid id, IPAddress ip, string user)
        {
            var data = await _retrieveByID(status, id, ip, user);
            return (data != null && data.document != null) ? data.document : null;
        }

        public override async Task<IEnumerable<modelLibroPatrono>> retrieveByStatus(signStatus status, byte[] page, IPAddress ip, string user)
        {
            var data = await _retrieveByStatus(status, page, ip, user);
            return data.Select(x => (modelLibroPatrono)x.document);
        }

        public override async Task signDocument(modelLibroPatrono document, Guid id, IPAddress ip, string user)
        {
            var doc = new entityDocumentByStatus(storeConnection, ip, user);
            await doc.removeDocument(signStatus.Unsigned, id);
            await doc.addDocument(signStatus.Signed, id, document);
        }
    }
}
