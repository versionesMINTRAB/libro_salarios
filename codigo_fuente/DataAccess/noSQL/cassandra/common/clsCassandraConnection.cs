﻿using Cassandra;
using DMLibroPatrono;
using EntityModel.noSQL.models.libroPatrono;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.noSQL.cassandra.common
{
    public sealed class clsCassandraConnection : IDocumentStoreConnection
    {
        private readonly Cluster cluster;
        private readonly string keyspace;
        public ISession session { get; private set; }

        private clsCassandraConnection(string keyspace)
        {
            this.keyspace = keyspace;
        }

        public clsCassandraConnection(string[] nodes, string keyspace, string user, string pwd) : this(keyspace)
        {
            cluster = Cluster
                .Builder()
                .AddContactPoints(nodes)
                .WithQueryTimeout(2400000)
                .WithCredentials(user, pwd)
                .Build();
            session = cluster.Connect(keyspace);
            mapping();
        }

        public clsCassandraConnection(string node, string keyspace, string user, string pwd) : this(keyspace)
        {
            cluster = Cluster
                .Builder()
                .AddContactPoint(node)
                .WithCredentials(user, pwd)
                .Build();
            session = cluster.Connect(keyspace);
            mapping();
        }

        private void mapping()
        {
            session.UserDefinedTypes.Define(
                UdtMap.For<identificacionLibroPatrono>(),
                UdtMap.For<persona>(),
                UdtMap.For<rawLibroPatrono>(),
                UdtMap.For<modelLibroPatrono>());
        }
    }
}
