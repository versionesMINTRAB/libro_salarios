﻿using Cassandra;

namespace DataAccess.noSQL.cassandra.common
{
    public interface IDocumentStoreConnection
    {
        ISession session { get; }
    }
}
