﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using DMLibroPatrono;
using EntityModel.noSQL.models.documentFlow;
using EntityModel.noSQL.models.libroPatrono;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.documentFlow
{
    public sealed class entityDocumentLog : cassandraEntity<documentLog>
    {
        public entityDocumentLog(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        private static documentLog documentFactory(Guid id, int newStatus, string note, IPAddress ip, string user)
        {
            return new documentLog
            {
                tkn = id,
                status = newStatus,
                note = note,
                ip = ip,
                user = user,
                fecha = DateTime.UtcNow
            };
        }

        private async Task addLog(documentLog document)
        {
            var applied = await mapper.InsertIfNotExistsAsync(document);
            if (!applied.Applied)
            {
                throw new Exception("Document already exists with this token (" + document.tkn.ToString() + "), can't be inserted");
            }
        }

        public async Task addLibroPatrono(Guid id, int newStatus, string note, IPAddress ip, string user)
        {
            var doc = documentFactory(id, newStatus, note, ip, user);
            await addLog(doc);
        }

        public async Task addLibroPatrono(Guid id, int newStatus, string note, IPAddress ip, string user, modelLibroPatrono libroPatrono)
        {
            var doc = documentFactory(id, newStatus, note, ip, user);
            doc.document = libroPatrono;
            await addLog(doc);
        }
    }
}
