﻿using Cassandra.Mapping;
using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using DMLibroPatrono;
using DMLibroPatrono.enums;
using EntityModel.noSQL.models.documentFlow;
using EntityModel.noSQL.models.libroPatrono;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.documentFlow
{
    public sealed class entityDocumentByStatus : cassandraEntity<documentByStatus>
    {
        private readonly entityDocumentLog log;
        private readonly IPAddress ip;
        private readonly string user;

        public entityDocumentByStatus(Func<IDocumentStoreConnection> connection, IPAddress ip, string user) : base(connection)
        {
            log = new entityDocumentLog(connection);
            this.ip = ip;
            this.user = user;
        }

        private async Task add(signStatus status, Guid id, documentByStatus document)
        {
            var applied = await mapper.InsertIfNotExistsAsync(document);
            if (!applied.Applied)
            {
                throw new Exception("Document already exists with this token (" + id.ToString() + "), can't be inserted");
            }
        }

        public async Task removeDocument(signStatus status, Guid id)
        {
            await mapper.DeleteAsync<documentByStatus>("where status = ? AND tkn = ?", new object[] { (int)status, id });
            await log.addLibroPatrono(id, (int)status, "Documento eliminado del cache", ip, user);
        }

        public async Task<IEnumerable<documentByStatus>> retrieveByStatus(signStatus status, byte[] page)
        {
            var tmp = await mapper.FetchPageAsync<documentByStatus>(Cql.New("where status = ?", (int)status).WithOptions(opt => opt.SetPageSize(100).SetPagingState(null)));
            return tmp;
        }

        public async Task<documentByStatus> retrieveByStatus(signStatus status, Guid id)
        {
            var data = await mapper.SingleOrDefaultAsync<documentByStatus>("where status = ? and tkn = ?", new object[] { (int)status, id });
            await log.addLibroPatrono(id, (int)status, "Documento consultado", ip, user);
            return data;
        }

        public async Task addDocument(signStatus status, Guid id, modelLibroPatrono libroPatrono)
        {
            await add(status, id, new documentByStatus()
            {
                status = (int)status,
                tkn = id,
                fecha = DateTimeOffset.Now,
                document = libroPatrono
            });
            await log.addLibroPatrono(id, (int)status, "Documento agregado", ip, user, libroPatrono);
        }
    }
}
