﻿using Cassandra.Mapping;
using DataAccess.noSQL.cassandra.common;
using System;

namespace DataAccess.noSQL.cassandra.entities.common
{
    public abstract class cassandraEntity<T> where T : class
    {
        protected IMapper mapper;
        protected readonly Func<IDocumentStoreConnection> connection;
        protected readonly IDocumentStoreConnection connInstance;

        public cassandraEntity(Func<IDocumentStoreConnection> connection)
        {
            this.connection = connection;
            connInstance = connection();
            mapper = new Mapper(connInstance.session);
        }
    }
}
