﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using EntityModel.noSQL.models.document;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.document
{
    public sealed class entityDocumentoDescargas : cassandraEntity<documentoDescargas>
    {
        public entityDocumentoDescargas(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task<bool> increment(Guid id)
        {
            const long incr = 1;
            var result = await mapper.UpdateIfAsync<documentoDescargas>("SET downloads = downloads + ? WHERE id = ?", incr, id);
            return result.Applied;
        }

        public async Task<int> count(Guid id)
        {
            var data = await mapper.SingleOrDefaultAsync<documentoDescargas>("WHERE id = ?", id);
            return data != null ? data.downloads : 0;
        }

    }
}
