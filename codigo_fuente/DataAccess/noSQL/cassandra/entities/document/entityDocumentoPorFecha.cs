﻿using Cassandra.Mapping;
using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using EntityModel.noSQL.models.document;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.document
{
    public sealed class entityDocumentoPorFecha : cassandraEntity<documentoPorFecha>
    {
        public entityDocumentoPorFecha(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task addDocumento(documentoPorFecha documentoFirmado)
        {
            await mapper.InsertAsync(documentoFirmado);
        }
    }
}
