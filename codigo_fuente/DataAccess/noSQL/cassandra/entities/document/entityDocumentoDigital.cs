﻿using Cassandra.Data.Linq;
using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using EntityModel.noSQL.models.document;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.document
{
    public sealed class entityDocumentoDigital : cassandraEntity<documentoDigital>
    {
        public entityDocumentoDigital(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task<bool> addDocumento(Guid id, string filePath)
        {
            byte[] fileBytes = File.ReadAllBytes(filePath);
            var result = await mapper.InsertIfNotExistsAsync(new documentoDigital()
            {
                id = id,
                file = fileBytes
            });
            return result.Applied;
        }

        public async Task<documentoDigital> getDocumento(Guid id)
        {
            return await mapper.SingleOrDefaultAsync<documentoDigital>("WHERE id = ?", id);
        }

        public async Task removeDocument(Guid id)
        {
            await mapper.DeleteAsync<documentoDigital>("WHERE id = ?", id);
        }

        public async Task<long> existsDocumento(Guid id)
        {
            var tablePatentes = new Table<documentoDigital>(connInstance.session);
            return await (from pt in tablePatentes
                          where pt.id == id
                          select pt).Count().ExecuteAsync();
        }
    }
}
