﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using EntityModel.noSQL.models.document;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.document
{
    public sealed class entityDocumentoFirmado : cassandraEntity<documentoFirmado>
    {
        public entityDocumentoFirmado(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task<documentoFirmado> getDocumento(string user, int year, Guid id)
        {
            return await mapper.SingleOrDefaultAsync<documentoFirmado>("WHERE user = ? AND year = ? AND id = ?", new object[] { user, year, id });
        }

        public async Task addDocumento(documentoFirmado patenteFirmada)
        {
            await mapper.InsertAsync(patenteFirmada);
        }
    }
}
