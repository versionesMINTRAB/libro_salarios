﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using DMLibroPatrono;
using EntityModel.noSQL.models.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.batch
{
    public sealed class entityLoteDocumentos : cassandraEntity<loteDocumentos>
    {
        public entityLoteDocumentos(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task<IEnumerable<identificacionLibroPatrono>> documentosByBatchId(Guid id)
        {
            var data = await mapper.FetchAsync<loteDocumentos>("WHERE id = ?", id);
            return data.ToList().Select(x => (identificacionLibroPatrono)x);
        }

        public async Task addDocumento(Guid id, identificacionLibroPatrono documento)
        {
            await mapper.InsertAsync(new loteDocumentos()
            {
                id = id,
                key = new identificacionLibroPatrono()
                {
                    nit = documento.nit,
                    fecha = documento.fecha
                }
            });
        }
    }
}
