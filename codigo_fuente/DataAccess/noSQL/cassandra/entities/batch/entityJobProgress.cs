﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using EntityModel.noSQL.models.common;
using System;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.batch
{
    public sealed class entityJobProgress : cassandraEntity<jobProgress>
    {
        private const string cqlUpdate = "SET signeddocs = signeddocs + ? WHERE id = ?";
        private const string cqlGet = "WHERE id = ?";

        public entityJobProgress(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task<bool> increment(Guid id)
        {
            const long incr = 1;
            var result = await mapper.UpdateIfAsync<jobProgress>(cqlUpdate, incr, id);
            return result.Applied;
        }

        public async Task<int> count(Guid id)
        {
            var data = await mapper.SingleOrDefaultAsync<jobProgress>(cqlGet, id);
            return data != null ? data.signedDocs : 0;
        }
    }
}
