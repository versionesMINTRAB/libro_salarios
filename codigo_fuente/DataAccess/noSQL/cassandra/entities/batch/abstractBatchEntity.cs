﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.batch
{
    public abstract class abstractBatchEntity<T> : cassandraEntity<T>
        where T : class
    {
        public abstractBatchEntity(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task addDocumento(T documento)
        {
            await mapper.InsertAsync(documento);
        }

        protected async Task<IEnumerable<T>> documentosByBatchId(Guid id)
        {
            return await mapper.FetchAsync<T>("WHERE batchid = ?", id);
        }
    }
}
