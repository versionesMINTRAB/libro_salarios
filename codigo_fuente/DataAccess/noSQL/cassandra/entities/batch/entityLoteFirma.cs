﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using EntityModel.noSQL.models.common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.batch
{
    public sealed class entityLoteFirma : cassandraEntity<loteFirma>
    {
        public entityLoteFirma(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task<loteFirma> getFirma(Guid id)
        {
            return await mapper.SingleOrDefaultAsync<loteFirma>("WHERE id = ?", id);
        }

        public async Task addFirma(loteFirma firma)
        {
            await mapper.InsertAsync(firma);
        }
    }
}
