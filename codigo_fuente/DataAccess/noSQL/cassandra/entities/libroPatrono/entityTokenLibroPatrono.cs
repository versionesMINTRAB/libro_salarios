﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.batch;
using DMLibroPatrono;
using EntityModel.noSQL.models.libroPatrono;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.libroPatrono
{
    public sealed class entityTokenLibroPatrono : abstractBatchEntity<tokenLibroPatrono>
    {
        public entityTokenLibroPatrono(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task<IEnumerable<modelTokenLibroPatrono>> getDataByID(identificacionLibroPatrono id)
        {
            var res = await mapper.FetchAsync<modelTokenLibroPatrono>("where nit=? and fecha=?", new object[] { id.nit, id.fecha });
            return res.ToList().Select(x => x);
        }
    }
}
