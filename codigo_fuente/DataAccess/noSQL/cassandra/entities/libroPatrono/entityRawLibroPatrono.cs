﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using DMLibroPatrono;
using EntityModel.noSQL.models.libroPatrono;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.libroPatrono
{
    public sealed class entityRawLibroPatrono : cassandraEntity<rawLibroPatrono>
    {
        public entityRawLibroPatrono(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task addModel(Guid id, modelLibroPatrono modelo)
        {

            var newEntity = new rawLibroPatrono()
            {
                tkn = id,
                document = modelo
            };
            var applied = await mapper.InsertIfNotExistsAsync(newEntity);
            if (!applied.Applied)
            {
                throw new Exception("Document already exists with this token (" + id.ToString() + "), can't be inserted");
            }
        }

        public async Task<modelLibroPatrono> getModelAsJSON(Guid id)
        {
            var data = await mapper.SingleOrDefaultAsync<rawLibroPatrono>("where tkn = ?", id);
            return (data != null && data.document != null) ? data.document : null;
        }
    }
}
