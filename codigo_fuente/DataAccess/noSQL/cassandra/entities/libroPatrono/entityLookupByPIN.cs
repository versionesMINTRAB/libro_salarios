﻿using DataAccess.noSQL.cassandra.common;
using DataAccess.noSQL.cassandra.entities.common;
using DataAccess.noSQL.cassandra.entities.document;
using DMLibroPatrono;
using EntityModel.noSQL.models.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.noSQL.cassandra.entities.libroPatrono
{
    public sealed class entityLookupByPIN : cassandraEntity<lookupByPIN>
    {
        public entityLookupByPIN(Func<IDocumentStoreConnection> connection) : base(connection)
        {
        }

        public async Task addPIN(modelSinglePIN modelPIN)
        {
            await mapper.InsertAsync(new lookupByPIN()
            {
                pin = modelPIN.pin,
                nit = modelPIN.libroPatrono.nit,
                fecha = DateTimeOffset.Now,
                libroPatrono = modelPIN.libroPatrono
            });
        }

        public async Task<Tuple<modelLibroPatrono,Guid?, bool>> lookup(modelSinglePIN modelPIN)
        {
            var data = await mapper.FirstOrDefaultAsync<lookupByPIN>("where pin = ? and nit=?", new object[] { modelPIN.pin, modelPIN.libroPatrono.nit });
            if (data != null)
            {
                var rawData = new entityTokenLibroPatrono(connection);
                var res = await rawData.getDataByID(modelPIN.libroPatrono);
                if (res!=null && res.Any())
                {
                    var rawLibroPatrono = new entityRawLibroPatrono(connection);
                    var modelLibroPatrono = await rawLibroPatrono.getModelAsJSON(res.Last().id);
                    return new Tuple<modelLibroPatrono, Guid?, bool>(modelLibroPatrono, res.Last().id, true);
                }
            }
            return new Tuple<modelLibroPatrono, Guid?, bool>(null, null, false);
        }
    }
}
