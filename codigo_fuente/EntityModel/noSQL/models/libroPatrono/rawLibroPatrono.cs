﻿using DMLibroPatrono;
using EntityModel.noSQL.models.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityModel.noSQL.models.libroPatrono
{
    public class rawLibroPatrono : documentHolder<modelLibroPatrono>
    {
        public Guid tkn { get; set; }
    }
}
