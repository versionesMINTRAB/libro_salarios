﻿using EntityModel.noSQL.models.libroPatrono;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace EntityModel.noSQL.models.documentFlow
{
    public sealed class documentLog : rawLibroPatrono
    {
        public DateTimeOffset fecha { get; set; }
        public int status { get; set; }
        public string note { get; set; }
        public IPAddress ip { get; set; }
        public string user { get; set; }
    }
}
