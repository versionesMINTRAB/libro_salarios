﻿using DMLibroPatrono;
using EntityModel.noSQL.models.libroPatrono;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityModel.noSQL.models.documentFlow
{
    public sealed class documentByStatus : rawLibroPatrono
    {
        public int status { get; set; }
        public DateTimeOffset fecha { get; set; }
    }
}
