﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityModel.noSQL.models.document
{
    public sealed class documentoPorFecha
    {
        public string user { get; set; }
        public DateTimeOffset fechaFirma { get; set; }
        public Guid id { get; set; }
    }
}
