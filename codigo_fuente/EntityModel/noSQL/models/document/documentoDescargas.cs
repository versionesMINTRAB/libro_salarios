﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityModel.noSQL.models.document
{
    public sealed class documentoDescargas
    {
        public Guid id { get; set; }
        public int downloads { get; set; }
    }
}
