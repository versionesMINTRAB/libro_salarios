﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace EntityModel.noSQL.models.document
{
    public sealed class documentoFirmado
    {
        public string user { get; set; }
        public int year { get; set; }
        public Guid id { get; set; }
        public IPAddress ip { get; set; }
        public DateTimeOffset fechaFirma { get; set; }
        public int statusFirma { get; set; }
        public string descripcion { get; set; }
        public string firmante { get; set; }
        public string razon { get; set; }
        public bool wholeDocumentSigned { get; set; }
        public int currentRevision { get; set; }
        public int totalRevisions { get; set; }
        public bool isIntegral { get; set; }
        public Guid batchId { get; set; }
    }
}
