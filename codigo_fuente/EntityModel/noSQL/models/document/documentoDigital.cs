﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityModel.noSQL.models.document
{
    public sealed class documentoDigital
    {
        public Guid id { get; set; }
        public byte[] file { get; set; }
    }
}
