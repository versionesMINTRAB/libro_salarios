﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityModel.noSQL.models.common
{
    public abstract class abstractLoteDocumentos<TKey>
    {
        public Guid id { get; set; }
        public TKey key { get; set; }

        public static explicit operator TKey(abstractLoteDocumentos<TKey> x)
        {
            return x.key;
        }
    }
}
