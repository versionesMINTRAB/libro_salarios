﻿using DMLibroPatrono;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityModel.noSQL.models.common
{
    public sealed class lookupByPIN
    {
        public string pin { get; set; }
        public string nit { get; set; }
        public DateTimeOffset fecha { get; set; }
        public identificacionLibroPatrono libroPatrono { get; set; }
    }
}
