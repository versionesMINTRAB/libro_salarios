﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityModel.noSQL.models.common
{
    public sealed class jobProgress
    {
        public Guid id { get; set; }
        public int signedDocs { get; set; }
    }
}
