﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityModel.noSQL.models.common
{
    public abstract class documentHolder<TModel>
    {
        public TModel document { get; set; }
    }
}
