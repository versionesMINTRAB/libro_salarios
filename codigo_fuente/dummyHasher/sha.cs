﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace dummyHasher
{
    public static class sha
    {
        public static string PWDGenerator(string inputString)
        {
            int lengthOfPassword = 8;
            string pass = BitConverter.ToString(new SHA512CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(inputString))).Replace("-", string.Empty);
            return pass.Substring(0, lengthOfPassword);
        }

        public static string GenerateRSAString(string inputString)
        {
            var md5 = RSA.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = md5.Encrypt(bytes, RSAEncryptionPadding.Pkcs1);
            return GetStringFromHash(hash);
        }

        public static string GenerateMD5String(string inputString)
        {
            MD5 md5 = MD5.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = md5.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        public static string GenerateSHA1String(string inputString)
        {
            SHA1 sha1 = SHA1.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha1.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        public static string GenerateSHA256String(string inputString)
        {
            SHA256 sha256 = SHA256.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha256.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        public static string GenerateSHA512String(string inputString)
        {
            SHA512 sha512 = SHA512.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha512.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

    }
}
