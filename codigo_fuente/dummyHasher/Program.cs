﻿using PartialKeyVerification;
using PartialKeyVerification.Checksum;
using PartialKeyVerification.Hash;
using System;
using System.Security.Cryptography;
using System.Text;

namespace dummyHasher
{
    class Program
    {
        static void Main(string[] args)
        {
            var prueba = "9478795#" + DateTime.UtcNow;
            var generator = new PartialKeyGenerator(new Adler16(), new Jenkins96(), new uint[] { 1, 2, 3, 4 }) { Spacing = 6 };
            var key = generator.Generate(prueba);
            var sn = PartialKeyValidator.GetSerialNumberFromKey(key);
            

            var isValid = PartialKeyValidator.ValidateKey(new Adler16(), new Jenkins96(), key, 0, 8);

            Console.WriteLine(prueba);
            Console.WriteLine("key " + key + " is valid? " + isValid.ToString());
            Console.WriteLine("sn " + sn.ToString());
            Console.WriteLine("pwd gen");
            Console.WriteLine(sha.PWDGenerator(prueba));
            Console.WriteLine("rsa 1st");
            Console.WriteLine(sha.GenerateRSAString(prueba));
            Console.WriteLine("sha1 1st");
            Console.WriteLine(sha.GenerateSHA1String(prueba));
            Console.WriteLine("sha256 2nd");
            Console.WriteLine(sha.GenerateSHA256String(prueba));
            Console.WriteLine("sha512 3rd");
            Console.WriteLine(sha.GenerateSHA512String(prueba));
            Console.WriteLine("md5 4th");
            Console.WriteLine(sha.GenerateMD5String(prueba));
            Console.ReadKey();
        }

        public static string getHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
    }
}
