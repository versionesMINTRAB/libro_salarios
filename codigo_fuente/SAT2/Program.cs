﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAT2
{
    class program
    {
        static void Main(string[] args)
        {
            var sat = new wsSAT.ContribuyenteWsEpClient();
            sat.Endpoint.Address = new System.ServiceModel.EndpointAddress("https://farm2.sat.gob.gt/minegocio-ws/serviciosWeb/ConsultaMinegocio?wsdl");
            sat.Open();
            try
            {
                var result = sat.buscarNitActivo("W3440540", "557BD812DB", "45647763");
                var xml = new System.Xml.XmlDocument();
                xml.LoadXml(result);
                if (xml.SelectSingleNode("CONTRIBUYENTE/COD").InnerText.Equals("0") && xml.SelectSingleNode("CONTRIBUYENTE/NOM") != null)
                {
                    var nombre = xml.SelectSingleNode("CONTRIBUYENTE/NOM").InnerText;
                    var nit = xml.SelectSingleNode("CONTRIBUYENTE/NIT").InnerText;
                    Console.WriteLine(nombre);
                }
                else
                {
                    Console.WriteLine("NIT no encontrado");
                }
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }
            finally {
                sat.Close();
            }
        }
    }
}
