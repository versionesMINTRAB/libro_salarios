﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceBroker.common
{
    internal class constants
    {
        public const string DeadLetterExchangeTag = "x-dead-letter-exchange";
        public const string POISONPrefix = "POISON";
        public const string DeadLetterExchange = "pe.empresa.dlq";
    }
}
