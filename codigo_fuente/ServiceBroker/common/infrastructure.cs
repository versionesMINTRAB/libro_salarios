﻿using RabbitMQ.Client;
using ServiceBroker.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceBroker.common
{
    public abstract class infrastructure
    {
        protected IServiceBrokerConnection myConn;

        protected abstract void serviceDefinition(IModel channel);

        public infrastructure(IServiceBrokerConnection Conn)
        {
            myConn = Conn;
        }

        protected void serviceFallBack(IModel channel, string Name)
        {
            channel.ExchangeDeclare(Name, ExchangeType.Fanout, true);
            channel.QueueDeclare(Name, true, false, false, null);
            channel.QueueBind(Name, Name, string.Empty);
        }
    }
}
