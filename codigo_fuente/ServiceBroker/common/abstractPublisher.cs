﻿using commonModel.interfaces;
using Newtonsoft.Json;
using ServiceBroker.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceBroker.common
{
    public abstract class abstractPublisher<T> : abstractService
       where T : class, IMessage
    {
        public abstractPublisher(IServiceBrokerConnection conn, string exchangeName) : base(conn)
        {
            this.exchangeName = exchangeName;
        }

        protected string exchangeName { get; }

        protected void send(string routingKey, T Message)
        {
            try
            {
                string json = JsonConvert.SerializeObject(Message, Formatting.None);
                send(routingKey, json);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void send(string routingKey, T Message, string replyToQueue)
        {
            string json = JsonConvert.SerializeObject(Message, Formatting.None);
            send(routingKey, json, replyToQueue);
        }

        private void send(string routingKey, string message)
        {
            using (var channel = conn.connection.CreateModel())
            {
                var body = Encoding.UTF8.GetBytes(message);
                var prop = channel.CreateBasicProperties();
                prop.Persistent = true;
                channel.ConfirmSelect();
                channel.BasicPublish(exchangeName, routingKey, false, prop, body);
                channel.WaitForConfirmsOrDie();
            }
        }

        private void send(string routingKey, string message, string replyToQueue)
        {
            using (var channel = conn.connection.CreateModel())
            {
                var body = Encoding.UTF8.GetBytes(message);
                var prop = channel.CreateBasicProperties();
                var corrId = Guid.NewGuid().ToString();
                prop.ReplyTo = replyToQueue;
                prop.CorrelationId = corrId;
                prop.Persistent = true;
                channel.ConfirmSelect();
                channel.BasicPublish(exchangeName, routingKey, false, prop, body);
                channel.WaitForConfirmsOrDie();
            }
        }
    }
}
