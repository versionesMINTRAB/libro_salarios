﻿using commonModel.interfaces;
using Newtonsoft.Json;
using RabbitMQ.Client;
using ServiceBroker.interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBroker.common
{

    public abstract class abstractSubscriberAsync<T> : abstractCoreSubscriber, ISubscriber
        where T : class, IMessage
    {
        private const string patenteElectronicaExchange = "patenteElectronica";

        public abstractSubscriberAsync(IServiceBrokerConnection conn) : base(conn)
        { }

        protected sealed override string exchangeName => patenteElectronicaExchange;

        protected abstract Task onMessage(T Message);

        protected string setIDRefence(T Message)
        {
            return Message.contexto.ToString();
        }

        protected virtual T deserialize(string message)
        {
            return message.Length > 0 ? JsonConvert.DeserializeObject<T>(message) : null;
        }

        public void startListener()
        {
            var consumer = initListener();
            consumer.Received += async (model, ea) =>
            {
                isProcessing.Wait();
                try
                {
                    isProcessed = false;
                    T MessageBody = deserialize(Encoding.UTF8.GetString(ea.Body));
                    try
                    {
                        await onMessage(MessageBody);
                        channel.BasicAck(ea.DeliveryTag, false);
                    }
                    catch (ApplicationException ex)
                    {
                        poisonMessage(ea, ex.ToString(), setIDRefence(MessageBody), serviceName, MessageBody.contexto);
                    }
                    catch (Exception ex)
                    {
                        poisonMessage(ea, ex.ToString(), setIDRefence(MessageBody), serviceName, MessageBody.contexto);
                    }
                }
                catch (RabbitMQ.Client.Exceptions.AlreadyClosedException) { }
                catch (RabbitMQ.Client.Exceptions.OperationInterruptedException) { }
                catch (System.IO.EndOfStreamException) { }
                catch (Exception ex) { generalException(ex); }
                finally
                {
                    releaseLock();
                }
            };
            channel.BasicConsume(queueName, false, serviceName, consumer);
        }
    }
}
