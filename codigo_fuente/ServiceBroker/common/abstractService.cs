﻿using ServiceBroker.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceBroker.common
{
    public abstract class abstractService
    {
        protected readonly IServiceBrokerConnection conn;

        protected internal abstractService(IServiceBrokerConnection Conn)
        {
            conn = Conn;
        }
    }
}
