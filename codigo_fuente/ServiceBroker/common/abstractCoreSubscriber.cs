﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceBroker.interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ServiceBroker.common
{
    public abstract class abstractCoreSubscriber : abstractService, IDisposable
    {
        protected IModel channel { get; set; }
        protected bool isProcessed;
        protected SemaphoreSlim isProcessing;

        public abstractCoreSubscriber(IServiceBrokerConnection conn) : base(conn)
        {
            isProcessing = new SemaphoreSlim(1, 1);
            isProcessed = true;
            channel = null;
        }

        protected abstract string serviceName { get; }
        protected abstract string queueName { get; }
        protected abstract string exchangeName { get; }
        protected abstract string routingKey { get; }

        protected virtual bool autoDelete => false;

        protected virtual string deadLetterExchange => constants.DeadLetterExchange;

        protected virtual void queueBinding() => channel.QueueBind(queueName, exchangeName, routingKey, null);

        protected virtual void generalException(Exception ex) => Console.WriteLine("General Error on " + serviceName + " handling: " + ex.ToString());

        /// <summary>
        /// Leveraging RabbitMQ Subscriber Infrastructure
        /// </summary>
        protected virtual void infrastructure()
        {
            var args = new Dictionary<string, object> { { constants.DeadLetterExchangeTag, deadLetterExchange } };
            channel.QueueDeclare(queueName, true, false, autoDelete, args);
        }

        protected EventingBasicConsumer initListener()
        {
            channel = conn.connection.CreateModel();
            channel.BasicQos(0, 1, false);
            infrastructure();
            queueBinding();
            return new EventingBasicConsumer(channel);
        }

        protected void releaseLock()
        {
            isProcessing.Release();
            isProcessed = true;
        }

        protected void poisonMessage(BasicDeliverEventArgs Msg, string error, string idReference, string serviceName, Guid Contexto)
        {
            try
            {
                log("Error en servicio" + serviceName + " ID: " + idReference + " Error:" + error);
                if (!isProcessed) channel.BasicNack(Msg.DeliveryTag, false, false);
            }
            catch (Exception)
            {
                log("Error en servicio" + serviceName + " ID: " + idReference + " Error:" + error);
            }
        }

        public static void log(string logMessage)
        {
            Console.Write("\r\nLog Entry : ");
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToLongDateString());
            Console.WriteLine("  :");
            Console.WriteLine("  :" + logMessage);
            Console.WriteLine("-------------------------------");
        }

        public void closeListener()
        {
            isProcessing.Wait();
            channel.Close();
            isProcessing.Release();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    isProcessing.Dispose(); if (channel != null) channel.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
