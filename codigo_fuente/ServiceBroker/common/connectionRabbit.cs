﻿using RabbitMQ.Client;
using ServiceBroker.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceBroker.common
{
    public sealed class connectionRabbit : IDisposable, IServiceBrokerConnection
    {
        private IConnection myConn { get; set; }
        private readonly string uri;

        public connectionRabbit(string uri)
        {
            myConn = null;
            this.uri = uri;
        }

        public IConnection connection
        {
            get
            {
                return myConn;
            }
        }

        public void connect()
        {
            Console.WriteLine("Estableciendo conexión con RabbitMQ en la uri (" + uri + ")");
            try
            {
                var factory = new ConnectionFactory()
                {
                    Uri = new Uri(uri),
                    RequestedHeartbeat = 10,
                    AutomaticRecoveryEnabled = true
                };
                myConn = factory.CreateConnection();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void closeConnection()
        {
            myConn.Close();
        }

        public void Dispose()
        {
            closeConnection();
        }
    }
}
