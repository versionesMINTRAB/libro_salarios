﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceBroker.interfaces
{
    public interface IServiceBrokerConnection
    {
        void connect();
        IConnection connection { get; }
    }
}
