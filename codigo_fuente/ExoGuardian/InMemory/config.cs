﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;
using static IdentityServer4.IdentityServerConstants;

namespace ExoGuardian.InMemory
{
    public sealed class config
    {
        public static IEnumerable<IdentityResource> getIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResource("roles", new List<string>{ JwtClaimTypes.Role })
            };
        }

        public static IEnumerable<ApiResource> getApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("Gilgamesh", "Backend RESTful Api")
                {
                    UserClaims=new List<string> { "roles" },
                },
                new ApiResource("api2", "IdentityManagement")
            };
        }

        public static IEnumerable<Client> getClients()
        {
            return new List<Client>
            {
                new Client {
                    ClientId = "Gilgamesh",
                    AllowedGrantTypes = GrantTypes.ImplicitAndClientCredentials,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("Secreto@3#2!2018".Sha256())
                    },
                },
                new Client {
                    ClientId = "Enkidu",
                    ClientName = "Inventario Frontend",
                    RequireConsent=false,
                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowAccessTokensViaBrowser = true,
                    ClientSecrets =
                    {
                        new Secret("Secreto@1#2!2018".Sha256())
                    },
                    // secret for authentication
                    AllowedCorsOrigins = { "http://localhost:8080" },
                    RedirectUris = { "http://localhost:8080/oidc-callback",
                                     "http://localhost:8080/silent-renew-oidc.html"},
                    PostLogoutRedirectUris = { "http://localhost:8080/index.html" },
                    AllowedScopes = new List<string>
                    {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        "roles",
                        "Gilgamesh"
                    },
                    IdentityTokenLifetime = 300
                },
                new Client
                {
                    ClientId = "ethos",
                    ClientName = "Identity Manager UI",
                    AllowedGrantTypes = GrantTypes.ImplicitAndClientCredentials,
                    AlwaysIncludeUserClaimsInIdToken=true,
                    AllowAccessTokensViaBrowser=true,
                    RequireConsent = false,
                    ClientSecrets =
                    {
                        new Secret("Secreto@6#2!2018".Sha256())
                    },
                    // where to redirect to after login
                    RedirectUris = { "https://localhost:7001/signin-oidc" },
                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "https://localhost:7001/signout-callback-oidc" },
                    AllowedScopes = new List<string>
                    {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        "roles", "api2"
                    },
                    AllowOfflineAccess = true
                }
            };
        }
    }
}
