## PROYECTO LIBRO DE SALARIOS

1. Codigo fuente del proyecto en carpeta **codigo_fuente**

---

## DOCKER PARA RABBITMQ 

- Esta imagen se contruye a partir de **Dockerfile**  contenido en la carpeta docker_rabbitMQ. 
- La base de la imagen es ALPINE 3.6
- Nombre de la imagen: rabbitmq:3.6-alpine


### PROCEDIMIENTO PARA CONSTRUIR LA IMAGEN E INICIAR EL SERVICIO

1.	Conocer la ruta del dockerfile.

2.	Construir la imagen:

	$ docker build -t rabbtitmq:3.6-alpine <ruta del dockefile>

3.	Iniciar el contenedor:

	$ docker run –d –p 9001:15671 –p 9002:15672 rabbitmq:3.6-alpine

4.	Para asignarle un directorio local al volumen del contendor:

	$ Docker run –d –p 9001:15671 –p 9002:15672 –v <directorio local>:<directorio del contedor> rabbitmq:3.6-alpine

---

## DOCKER PARA CASSANDRA

- Esta imagen construye desde el repositorio de docker: hub.docker.com
- La base de la imagen es: DEBIAN
- El nombre de la imagen es: cassandra:2

### PROCEDIMIENTO PARA CONTRUIR LA IMAGEN E INICIAR EL SERVICIO

1.	Descargar la imagen desde docker hub
	$ docker pull cassandra:2

2.	Iniciar el contenedor, asignando los puertos  y el volumen a utilizar:

	$ docker run –itd –p 8000:7000 –p 8001:7001 –p 8002:7199 –p 8003:9042 –p 8004:9160 –v **<directorio_local>**:**<directorio_del_contenedo>** cassandra:2

---

## DOCKER PARA POSTGRESQL

- Esta imagen construye desde el repositorio de docker: hub.docker.com
- La base de la imagen es: ALPINE:3.8
- El nombre de la imagen es: postgres:11.1-alpine

### PROCEDIMIENTO PARA CONTRUIR LA IMAGEN E INICIAR EL SERVICIO

1.	Descargar la imagen desde docker hub:

	$ docker pull postgres:11.1-alpine

2.	Iniciar el contenedor, asignando los puertos  y el volumen a utilizar:

	$ docker run –itd –p 6000:5432 –v **<directorio local>**:**<directorio del contenedor>** postgres:11.1-alpine

---

## DOCKERIZAR APLICACION .NET CORE

1.	Estar en la carpeta raiz de la aplicacion.
2.	Crear un documento **"Dockerfile"**. con lo siguiente:

		# FROM microsoft/dotnet:sdk AS build-env
		WORKDIR /app

		# Copy csproj and restore as distinct layers
		COPY *.csproj ./
		RUN dotnet restore

		# Copy everything else and build
		COPY . ./
		RUN dotnet publish -c Release -o out

		# Build runtime image
		FROM microsoft/dotnet:aspnetcore-runtime
		WORKDIR /app
		COPY --from=build-env /app/out .
		ENTRYPOINT ["dotnet", "aspnetapp.dll"]

	NOTA. Remplazar el nombre del archivo "aspnetapp.dll" con el arhivo generado para la aplicacion a doquerizar.


3. Construir la imagen docker a partir d este documento:
   
   	$ docker build -t aspnetapp .

4. Ejecutar y crear contenedor a partir de la imagen generada en el paso anterior:
	
	$ docker run -it --rm -p 8000:80 aspnetapp

---

### INICIAR TODO CON DOCKER-COMPOSE

1.	Para iniciar todo de forma automatica:

	$ docker-compose up

	**NOTA:** Modificar el archivo docker-compose.yml en el apartado de volumes para trabajar con volumenes compartidos fuera de los contenedores.